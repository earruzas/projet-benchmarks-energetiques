from typing import List

from argparse import Namespace
import argparse, os

import programs.FileTransfert.measures as measures_file_transfert
import programs.FileCompression.measures as measure_file_compression
import programs.DatabaseAccess.measures as measure_db_access
import programs.BinaryTrees.measures as measure_binary_trees

def available_programs() -> List[str]:
    return ['FileTransfert', 'FileCompression', 'DatabaseAccess', 'BinaryTrees']

def parse_args() -> Namespace:
    parser = argparse.ArgumentParser(
        description='This is the main script for doing benchmark measures'
    )

    parser.add_argument(
        'measures', type=str, choices=available_programs() + ['all'],
        help='The benchmark to run'
    )
    
    return parser.parse_args()
    
def main(args: Namespace):
    if os.getuid() != 0:
        print('Please run me as root for perf!')
        return

    out = f'measures/'

    os.makedirs(out, exist_ok=True)

    out = os.path.abspath(out)
    print(out)


    if args.measures == 'FileTransfert':
        measures_file_transfert.do_all_measures(out)
    elif args.measures == 'FileCompression':
        measure_file_compression.do_all_measures(out)
    elif args.measures == 'DatabaseAccess':
        measure_db_access.do_all_measures(out)
    elif args.measures == 'BinaryTrees':
        measure_binary_trees.do_all_measures(out)

if __name__ == "__main__":
    main(parse_args())