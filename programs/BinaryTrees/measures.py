import subprocess, time, csv, glob, os
from typing import Tuple

from utils import parse_joules_seconds
from utils import perf_command

def measure(command: str, depth: int) -> Tuple[float, float, float, float]:
    command = f'notfloc.py {command} {depth}'.split(' ')

    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    ret = process.wait()

    if ret != 0:
        print(' '.join(command))
        raise Exception("Ooops")

    data = process.stdout.read()
    out = parse_joules_seconds(data)
    print('Exec done: ', out)
    return out

def do_all_measures(out_dir: str):

    old = os.getcwd()
    os.chdir('programs/BinaryTrees/')

    commands = [
        './C/binarytrees',
        'java ./Java/binarytrees.java',
        'python3 ./Python/binarytrees.py'
    ]

    csv_file = open(f'{out_dir}/measures_binarytrees.csv', 'w', newline='')
    writer = csv.writer(csv_file, delimiter=';')
    writer.writerow(['language', 'cpu', 'ram', 'nic', 'sd', 'duration'])


    for command, language in zip(commands, ('C', 'Java', 'Python')):
        for _ in range(5):
            cpu, ram, nic, sd, duration = measure(command, 21)
            writer.writerow([language, f'{cpu}', f'{ram}', f'{nic}', f'{sd}' f'{duration}'])

    csv_file.close()
    
    os.chdir(old)
