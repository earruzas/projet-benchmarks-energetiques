#@title Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#@title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import os
import re
import shutil
import string
import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import losses

def main():

  print(tf.__version__)

  url = "https://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz"

  dataset = tf.keras.utils.get_file("aclImdb_v1", url,
                                      untar=True, cache_dir='.',
                                      cache_subdir='')

  dataset_dir = os.path.join(os.path.dirname(dataset), 'aclImdb')

  os.listdir(dataset_dir)
  train_dir = os.path.join(dataset_dir, 'train')
  os.listdir(train_dir)

  remove_dir = os.path.join(train_dir, 'unsup')
  shutil.rmtree(remove_dir)

  batch_size = 32
  seed = 42

  raw_train_ds = tf.keras.utils.text_dataset_from_directory(
      'aclImdb/train', 
      batch_size=batch_size, 
      validation_split=0.2, 
      subset='training', 
      seed=seed)

  raw_val_ds = tf.keras.utils.text_dataset_from_directory(
      'aclImdb/train', 
      batch_size=batch_size, 
      validation_split=0.2, 
      subset='validation', 
      seed=seed)

  raw_test_ds = tf.keras.utils.text_dataset_from_directory(
      'aclImdb/test', 
      batch_size=batch_size)

  def custom_standardization(input_data):
    lowercase = tf.strings.lower(input_data)
    stripped_html = tf.strings.regex_replace(lowercase, '<br />', ' ')
    return tf.strings.regex_replace(stripped_html,
                                    '[%s]' % re.escape(string.punctuation),
                                    '')

  max_features = 10000
  sequence_length = 250

  vectorize_layer = layers.TextVectorization(
      standardize=custom_standardization,
      max_tokens=max_features,
      output_mode='int',
      output_sequence_length=sequence_length)

  train_text = raw_train_ds.map(lambda x, y: x)
  vectorize_layer.adapt(train_text)

  def vectorize_text(text, label):
    text = tf.expand_dims(text, -1)
    return vectorize_layer(text), label

  train_ds = raw_train_ds.map(vectorize_text)
  val_ds = raw_val_ds.map(vectorize_text)
  test_ds = raw_test_ds.map(vectorize_text)

  AUTOTUNE = tf.data.AUTOTUNE

  train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
  val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
  test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

  embedding_dim = 16

  model = tf.keras.Sequential([
    layers.Embedding(max_features + 1, embedding_dim),
    layers.Dropout(0.2),
    layers.GlobalAveragePooling1D(),
    layers.Dropout(0.2),
    layers.Dense(1)])

  model.summary()

  model.compile(loss=losses.BinaryCrossentropy(from_logits=True),
                optimizer='adam',
                metrics=[tf.metrics.BinaryAccuracy()])

  epochs = 10
  model.fit(
      train_ds,
      validation_data=val_ds,
      epochs=epochs)

  model.save('export_model.h5')
  print("Model saved!")   
  loss, accuracy = model.evaluate(test_ds)

  print("Loss: ", loss)
  print("Accuracy: ", accuracy)

if __name__ == "__main__":
    main()