#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <sqlite3.h>
#include <string.h>
#include <mysql/mysql.h>

#include "users.h"

const char* request1_sql = 
    "SELECT DISTINCT u.* FROM User u, Tweet t\n"
    "WHERE t.like_count > ? AND\n"
    "t.retweet_count > ? AND\n"
    "t.lang = ? AND\n"
    "t.user_id = u.user_id";

void get_users(struct User* users, MYSQL_STMT* stmt)
{
    MYSQL_BIND res[6];

    uint64_t userid;
    char username[101];
    long followers_count, following_count;
    long tweet_count, verified;

    memset(res, 0, sizeof res);
    res[0].buffer_type = MYSQL_TYPE_LONGLONG;
    res[0].buffer = (char*)&userid;

    res[1].buffer_type = MYSQL_TYPE_STRING;
    res[1].buffer = username;
    res[1].buffer_length = 101;

    res[2].buffer_type = MYSQL_TYPE_LONG;
    res[2].buffer = (char*)&followers_count;

    res[3].buffer_type = MYSQL_TYPE_LONG;
    res[3].buffer = (char*)&following_count;

    res[4].buffer_type = MYSQL_TYPE_LONG;
    res[4].buffer = (char*)&tweet_count;

    res[5].buffer_type = MYSQL_TYPE_LONG;
    res[5].buffer = (char*)&verified;


    mysql_stmt_bind_result(stmt, res);

    while (! mysql_stmt_fetch(stmt))
    {
        struct User* user = user_add(users);
        user->user_id = userid;
        user->followers_count = followers_count;
        user->following_count = following_count;
        user->tweet_count = tweet_count;
        user->verified = verified;
        strncpy(user->username, username, 50);
    }
}

void get_tweets(struct Tweet* tweets, MYSQL_STMT* stmt)
{
    MYSQL_BIND res[7];

    uint64_t tweetid, userid;
    char text[300];
    long like_count = 0, retweet_count = 0, reply_count = 0;
    char lang[5]; 

    memset(res, 0, sizeof res);
    res[0].buffer_type = MYSQL_TYPE_LONGLONG;
    res[0].buffer = (char*)&tweetid;

    res[1].buffer_type = MYSQL_TYPE_STRING;
    res[1].buffer = text;
    res[1].buffer_length = 300;

    res[2].buffer_type = MYSQL_TYPE_LONG;
    res[2].buffer = (char*)&like_count;

    res[3].buffer_type = MYSQL_TYPE_LONG;
    res[3].buffer = (char*)&retweet_count;

    res[4].buffer_type = MYSQL_TYPE_LONG;
    res[4].buffer = (char*)&reply_count;

    res[5].buffer_type = MYSQL_TYPE_STRING;
    res[5].buffer = lang;
    res[5].buffer_length = 5;

    res[6].buffer_type = MYSQL_TYPE_LONGLONG;
    res[6].buffer = (char*)&userid;


    mysql_stmt_bind_result(stmt, res);
    
    while (! mysql_stmt_fetch(stmt))
    {
        struct Tweet* tweet = tweet_add(tweets);
        tweet->user_id = userid;
        tweet->tweet_id = tweetid;
        tweet->reply_count = reply_count;
        tweet->retweet_count = retweet_count;
        tweet->like_count = like_count;

        strncpy(tweet->lang, lang, 4);
        strncpy(tweet->text, text, 299);
    }
}

/**
 * Retreives all users with at least one tweet with more than x likes in the y lang
*/
void single_request(MYSQL* connection, int x, int y, const char* lang)
{
    MYSQL_STMT* stmt = mysql_stmt_init(connection);
    if (stmt == NULL)
    {
        fprintf(stderr, "Failed to instantiate prepared statement\n");
        return;
    }

    mysql_stmt_prepare(stmt, request1_sql, strlen(request1_sql));

    MYSQL_BIND bind[3];
    
    memset(bind, 0, sizeof bind);
    bind[0].buffer_type = MYSQL_TYPE_LONG;
    bind[0].buffer = (char*)&x;

    bind[1].buffer_type = MYSQL_TYPE_LONG;
    bind[1].buffer = (char*)&y;


    bind[2].buffer_type = MYSQL_TYPE_STRING;
    bind[2].buffer = lang;
    bind[2].buffer_length = strlen(lang);

    mysql_stmt_bind_param(stmt, bind);
    if (mysql_stmt_execute(stmt) != 0)
    {
        fprintf(stderr, "Failed to execute statement\n");
        return;
    }

   get_users(g_users, stmt);

    mysql_stmt_close(stmt);
}

void multiple_requests(MYSQL* connection, int x, int y, const char* lang)
{
    MYSQL_STMT* stmt = mysql_stmt_init(connection);
    if (stmt == NULL)
    {
        fprintf(stderr, "Failed to instantiate prepared statement\n");
        return;
    }

    const char* request = "SELECT DISTINCT u.* from User u, Tweet t where t.user_id = u.user_id and t.lang = ?";

    mysql_stmt_prepare(stmt, request, strlen(request));

    MYSQL_BIND bind[1];

    memset(bind, 0, sizeof bind);
    bind[0].buffer_type = MYSQL_TYPE_STRING;
    bind[0].buffer = lang;
    bind[0].buffer_length = strlen(lang);

    mysql_stmt_bind_param(stmt, bind);

    if (mysql_stmt_execute(stmt) != 0)
    {
        fprintf(stderr, "Failed to execute statement\n");
        return;
    }

    struct User* temp_users;
    user_init(&temp_users);

    get_users(temp_users, stmt);
    printf("Users : %ld\n", temp_users->count);
    mysql_stmt_close(stmt);

    struct User* user = temp_users->next;

    while (user->next != NULL)
    {
        const char* tweets_request = "SELECT distinct t.* from Tweet t where ? = t.user_id";
        
        MYSQL_STMT* stmt1 = mysql_stmt_init(connection);
        mysql_stmt_prepare(stmt1, tweets_request, strlen(tweets_request));

        memset(bind, 0, sizeof bind);
        bind[0].buffer_type = MYSQL_TYPE_LONGLONG;
        bind[0].buffer = (char*)&user->user_id;

        mysql_stmt_bind_param(stmt1, bind);

        if (mysql_stmt_execute(stmt1) != 0)
        {
            fprintf(stderr, "Failed to execute statement\n");
            return;
        }

        struct Tweet* tweets;
        tweet_init(&tweets);
        get_tweets(tweets, stmt1);
        mysql_stmt_close(stmt1);

        struct Tweet* tweet = tweets->next;
        while (tweet != NULL)
        {
            if (strcmp(tweet->lang, lang) == 0 && tweet->like_count > x && tweet->retweet_count > y)
            {
                printf("%ld - %ld\n", tweet->like_count, tweet->retweet_count);
                user_add(g_users);
                break;
            }

            tweet = tweet->next;
        }

        tweet_delete(tweets);
        user = user->next;
    }


}

int main(int argc, const char* argv[])
{
    if (argc < 2)
    {
        printf("Usage: %s [single|multiple]", argv[0]);
        return 1;
    }
    user_init(&g_users);

    MYSQL* connection;
    if (! (connection = mysql_init(0)))
    {
        return 1;
    }

    if (! mysql_real_connect(connection, "localhost", "mysql", "mysql", "tweetsdb", 3306, NULL, 0))
    {   
        mysql_close(connection);
        return 1;
    }

    if (strcmp(argv[1], "single") == 0)
        single_request(connection, 10000, 1000, "fr");
    else
        multiple_requests(connection, 10000, 1000, "fr");

    printf("Retrieved %ld users\n", g_users->count);

    mysql_close(connection);

    user_delete(g_users);
    return 0;
}

// gcc -o main main_mariadb.c users.c -lsqlite3 -lmysqlclient