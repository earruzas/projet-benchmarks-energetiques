#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <neo4j-client.h>

#include "users.h"

#define URI "neo4j://neo4j:tweetsdb@127.0.0.1:7687"

const char* request1_cypher = 
        "MATCH (u:User)-[:POSTED]->(t:Tweet)\n" 
        "WHERE t.like_count > $likeCount AND t.retweet_count > $retweetCount AND t.lang = $language\n" 
        "RETURN DISTINCT u";

static __always_inline uint64_t get_int(neo4j_result_t* result, unsigned int idx)
{
    neo4j_value_t value = neo4j_result_field(result, idx);
    char* out = malloc(50);
    neo4j_string_value(value, out, 50);
    char* end;
    uint64_t ret = strtoll(out, &end, 10);
    free(out);

    return ret;
}

static __always_inline uint64_t get_real_int(neo4j_result_t* result, unsigned int idx)
{
    neo4j_value_t value = neo4j_result_field(result, idx);

    return neo4j_int_value(value);
}

static __always_inline char* get_string(neo4j_result_t* result, unsigned int idx)
{
    neo4j_value_t value = neo4j_result_field(result, idx);
    char* out = malloc(50);
    neo4j_tostring(value, out, 50);
    
    return out;
}

void get_users(neo4j_result_stream_t* results, struct User* users)
{
    neo4j_result_t* result = neo4j_fetch_next(results);
    if (result == NULL)
        return ;
    for (;;)
    {
        struct User* user = user_add(users);
        user->user_id = get_int(result, 0);

        char* temp = get_string(result, 1);
        strncpy(user->username, temp, 50);
        free(temp);

        user->followers_count = get_int(result, 2);
        user->following_count = get_int(result, 3);
        user->tweet_count = get_int(result, 4);
        user->verified = (bool) get_int(result, 5);

        result = neo4j_fetch_next(results);
        if (result == NULL)
            break;
    }
}

void get_tweet(neo4j_result_stream_t* results, struct Tweet* tweets)
{
    neo4j_result_t* result = neo4j_fetch_next(results);
    if (result == NULL)
        return ;

    for (;;)
    {
        struct Tweet* tweet = tweet_add(tweets);
        tweet->tweet_id = get_real_int(result, 0);

        char* temp = get_string(result, 1);
        strncpy(tweet->text, temp, 50);
        free(temp);

        tweet->like_count = get_real_int(result, 2);
        tweet->retweet_count = get_real_int(result, 3);
        tweet->reply_count = get_real_int(result, 4);

        temp = get_string(result, 5);
        strncpy(tweet->lang, temp, 5);
        free(temp);

        tweet->user_id = get_real_int(result, 6);

        result = neo4j_fetch_next(results);
        if (result == NULL)
            break;
    }
}

void single_request(neo4j_connection_t* connection, int x, int y, const char* lang)
{
    neo4j_map_entry_t entries[] = {
        { .key = neo4j_string("likeCount"), .value = neo4j_int(x) },
        { .key = neo4j_string("language"), .value = neo4j_string(lang) },
        { .key = neo4j_string("retweetCount"), .value = neo4j_int(y)}
    };

    neo4j_value_t parameters = neo4j_map(entries, 3);

    neo4j_result_stream_t* results =
            neo4j_run(connection, request1_cypher, parameters);

    if (results == NULL)
    {
        neo4j_perror(stderr, errno, "Failed to run statement");
        return;
    }

    get_users(results, g_users);

    end:
        neo4j_close_results(results);
}

void multiple_requests(neo4j_connection_t* connection, int x, int y, const char* lang)
{
    neo4j_map_entry_t entries[] = {
        { .key = neo4j_string("language"), .value = neo4j_string(lang) },
    };

    neo4j_value_t parameters = neo4j_map(entries, 1);

    const char* request = "MATCH (u:User)-[:POSTED]->(t:Tweet)\n" 
        "WHERE t.lang = $language\n" 
        "RETURN DISTINCT u.user_id, u.username, u.followers_count, u.following_count, u.tweet_count, u.verified";

    neo4j_result_stream_t* results =
            neo4j_run(connection, request, parameters);

    if (results == NULL)
    {
        neo4j_perror(stderr, errno, "Failed to run statement");
        return;
    }


    struct User* temp_users;
    user_init(&temp_users);

    get_users(results, temp_users);

    neo4j_close_results(results);

    struct User* user = temp_users->next;

    char langneo4j[8];
    snprintf(langneo4j, 8, "\"%s\"", lang);

    while (user->next != NULL)
    {
        char user_id[100];
        snprintf(user_id, 100, "%llu", user->user_id);

        neo4j_connection_t* conn = neo4j_connect(URI, NULL, NEO4J_INSECURE);
        neo4j_map_entry_t entry[] = {
            {.key = neo4j_string("userId"), .value = neo4j_string(user_id)}
        };

        parameters = neo4j_map(entry, 1);

        const char* subrequest = "MATCH (u: User)-[:POSTED]->(t: Tweet)\n"
            "WHERE u.user_id = $userId\n"
            "RETURN t.tweet_id, t.text, t.like_count, t.retweet_count, t.reply_count, t.lang, t.user_id";


        results = neo4j_run(conn, subrequest, parameters);
        if (results == NULL)
        {
            neo4j_perror(stderr, errno, "Failed to run statement");
            return;
        }

        struct Tweet* tweets;
        tweet_init(&tweets);

        get_tweet(results, tweets);

        struct Tweet* tweet = tweets->next;
        while (tweet != NULL)
        {
            if (strcmp(tweet->lang, langneo4j) == 0 && tweet->like_count > x && tweet->retweet_count > y)
            {
                user_add(g_users);
                break;
            }
            tweet = tweet->next;
        }
        tweet_delete(tweets);

        neo4j_close_results(results);
        neo4j_close(conn);
        user = user->next;
    }

    user_delete(temp_users);

}

int main(int argc, const char* argv[])
{
    if (argc < 2)
    {
        printf("Usage: %s [single|multiple]\n", argv[0]);
        return EXIT_FAILURE;
    }

    user_init(&g_users);
    neo4j_client_init();

    neo4j_connection_t* connection = neo4j_connect(URI, NULL, NEO4J_INSECURE);
    if (connection == NULL)
    {
        neo4j_perror(stderr, errno, "Error: failed to make connection to neo4j db");
        return EXIT_FAILURE;
    }

    if (strcmp(argv[1], "single") == 0)
        single_request(connection, 10000, 1000, "fr");
    else
        multiple_requests(connection, 10000, 1000, "fr");


    printf("Retrieved: %lu users\n", g_users->count);


    user_delete(g_users);
    neo4j_close(connection);
    neo4j_client_cleanup();


    return EXIT_SUCCESS;
}

// gcc -o main main_neo4j.c users.c -lneo4j-client -static -lssl -lcrypto -lm
// working libneo4j-client with bolt-v5 as of 29/02/2024 :
// - https://github.com/johannessen/libneo4j-omni/tree/bolt-v5