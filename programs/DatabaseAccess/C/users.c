#include <stdlib.h>

#include "users.h"

struct User* g_users = NULL;

/**
 * Add a new user to the linked list and returns it
*/
struct User* user_add(struct User* users)
{
    struct User* user = users;
    user->count += 1;
    while (user->next != NULL)
    {
        user = user->next;
    }

    struct User* new = malloc(sizeof(struct User));
    new->next = NULL;
    user->next = new;

    return new;
}

/**
 * Init the users list
*/
void user_init(struct User** user)
{
    struct User* temp = malloc(sizeof(struct User));
    temp->next = NULL;
    temp->count = 0;
    *user = temp;
}

/**
 * Delete the users list (free)
*/
void user_delete(struct User* users)
{
    struct User* user = users;

    while (user->next != NULL)
    {
        struct User* temp = user->next;
        free(user);
        user = temp;
    }
    free(user);
}

struct Tweet* tweet_add(struct Tweet* tweets)
{
    struct Tweet* tweet = tweets;
    tweet->count += 1;
    while (tweet->next != NULL)
    {
        tweet = tweet->next;
    }

    struct Tweet* new = malloc(sizeof(struct Tweet));
    new->next = NULL;
    tweet->next = new;

    return new;
}

void tweet_init(struct Tweet** tweet)
{
    struct Tweet* temp = malloc(sizeof(struct Tweet));
    temp->next = NULL;
    temp->count = 0;
    *tweet = temp;
}

void tweet_delete(struct Tweet* tweets)
{
    struct Tweet* tweet = tweets;

    while (tweet->next != NULL)
    {
        struct Tweet* temp = tweet->next;
        free(tweet);
        tweet = temp;
    }
    free(tweet);
}