#pragma once

#include <stdint.h>
#include <stdbool.h>

#define uint128_t __uint128_t

struct User {
    uint128_t user_id;
    char username[50];
    uint64_t followers_count;
    uint64_t following_count;
    uint64_t tweet_count;
    bool verified;
    struct User* next;
    uint64_t count;
};

struct Tweet {
    uint128_t tweet_id;
    char text[500];
    uint64_t like_count;
    uint64_t retweet_count;
    uint64_t reply_count;
    char lang[5];
    uint128_t user_id;
    struct Tweet* next;
    uint64_t count;
};

extern struct User* g_users;

struct User* user_add(struct User* users);
void user_init(struct User** user);
void user_delete(struct User* users);

struct Tweet* tweet_add(struct Tweet* tweets);
void tweet_init(struct Tweet** tweet);
void tweet_delete(struct Tweet* tweets);