import java.util.Map;
import java.math.BigInteger; 
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.QueryConfig;
import org.neo4j.driver.Record;
import org.neo4j.driver.RoutingControl;
import org.neo4j.driver.SessionConfig;
import org.neo4j.driver.TransactionContext;
import org.neo4j.driver.exceptions.NoSuchRecordException;

import java.util.ArrayList;
import java.util.List;

/*
* to compile : (with drivers: Neo4j-java-driver-4.3.1.jar)
* javac -cp Drivers2/*:. MainNeo4j.java 
  java -cp Drivers3/*:. MainNeo4j tweetsdb
*/
public class MainNeo4j {

        public static class User {
                private String user_id;
                private String username;
                private String followers_count;
                private String following_count;
                private String tweet_count;
                private String verified;

            public User(String user_id, String username, String followers_count, String following_count, String tweet_count, String verified) {
                    this.user_id = user_id;
                    this.username = username;
                    this.followers_count = followers_count;
                    this.following_count = following_count;
                    this.tweet_count = tweet_count;
                    this.verified = verified;
                }
        }

        public static class Tweet {
            private long like_count;
            private String user_id;
            private long tweet_id;
            private String text;
            private long reply_count;
            private String lang;
            private long retweet_count;
        
            public Tweet(long like_count, String user_id, long tweet_id, String text, long reply_count, String lang, long retweet_count) {
                this.like_count = like_count;
                this.user_id = user_id;
                this.tweet_id = tweet_id;
                this.text = text;
                this.reply_count = reply_count;
                this.lang = lang;
                this.retweet_count = retweet_count;
            }
        }

        
        private static List<User> g_users = new ArrayList<>();
        private static List<User> g_user_temp = new ArrayList<>();

        private static final String REQUEST1_NEO =
            "MATCH (u:User)-[:POSTED]->(t:Tweet)\n" +
            "WHERE t.like_count > $likeCount\n"+
            "AND t.retweet_count > $rtCount\n"+
            "AND t.lang = $language\n"+
            "RETURN DISTINCT u";
   
        private static final String REQUEST2_NEO =
           "MATCH (u:User)-[:POSTED]->(t:Tweet)\n"+
           "WHERE t.user_id = u.user_id\n"+
           "AND t.lang = $language \n"+
           "RETURN DISTINCT u\n";

        private static final String REQUEST3_NEO = 
            "MATCH (u:User)-[:POSTED]->(t:Tweet)\n"+
            "WHERE t.user_id = $userid\n"+
            "RETURN DISTINCT t, id(t) AS identity\n";
        

        public static void main(String[] args) {
            if (args.length < 1) {
                System.out.println("Usage: [single|multiple]");
                return;
            }

            String requestType = args[0].toLowerCase();
            if (!requestType.equals("single") && !requestType.equals("multiple") && !requestType.equals("tweet")) {
                System.out.println("Invalid request type. Usage: [single|multiple]");
                return;
            }

            // URI examples: "neo4j://localhost", "neo4j+s://xxx.databases.neo4j.io"
            final String dbUri = "bolt://127.0.0.1:7687";
            final String dbUser = "neo4j";
            final String dbPassword = "tweetsdb";

            try (var driver = GraphDatabase.driver(dbUri, AuthTokens.basic(dbUser, dbPassword))) {
                try (var session = driver.session(SessionConfig.builder().withDatabase("tweetsdb").build())){
                    if (requestType.equals("single")) {
                        single_request(driver, 10000, 1000, "fr"); 
                    } else {
                        multiple_requests(driver, 10000, 1000, "fr"); 
                    }
                    System.out.println("Retrieved: " + g_users.size());
            }
            } catch (Exception e) {
                System.err.println("Error occurred: " + e.getMessage());
                e.printStackTrace();
            }
        }
    
        public static void single_request(org.neo4j.driver.Driver driver, long x, long y, String lang) {
            var result = driver.executableQuery(REQUEST1_NEO)
                            .withParameters(Map.of("likeCount", x, "rtCount", y,  "language", lang))
                            .execute();
                        
            var records = result.records();
            records.forEach(u -> {
                try {
                   
                    var node = u.get("u").asNode();

                    String userId = node.get("user_id").asString();
                    String username = node.get("username").asString();
                    String followersCount = node.get("followers_count").asString();
                    String followingCount = node.get("following_count").asString();
                    String tweetCount = node.get("tweet_count").asString();
                    String verified = node.get("verified").asString();

                    User user = new User(userId, username, followersCount, followingCount, tweetCount, verified);
                    g_users.add(user);

                } catch (Exception e) {
                    System.err.println("Error occurred while processing a record: " + e.getMessage());
                    e.printStackTrace();
                }
            });
       }
    
       public static void multiple_requests(org.neo4j.driver.Driver driver, long x, long y, String lang) {
        try {
            var result = driver.executableQuery(REQUEST2_NEO)
                               .withParameters(Map.of("language", lang))
                               .execute();

            var records = result.records();
                 
            records.forEach(u -> {
                try {
                    var node = u.get("u").asNode();
    
                    String userId = node.get("user_id").asString();
                    String username = node.get("username").asString();
                    String followersCount = node.get("followers_count").asString();
                    String followingCount = node.get("following_count").asString();
                    String tweetCount = node.get("tweet_count").asString();
                    String verified = node.get("verified").asString();
    
                    User user = new User(userId, username, followersCount, followingCount, tweetCount, verified);
                    g_user_temp.add(user);
                } catch (Exception e) {
                    System.err.println("Error first loop : " + e.getMessage());
                    e.printStackTrace();
                }
            });
        
            for (User user : g_user_temp) {
                var result3 = driver.executableQuery(REQUEST3_NEO)
                                    .withParameters(Map.of("userid", user.user_id))
                                    .execute();
                
                var records3 = result3.records();
                boolean hasValidTweet = false;

                if (records3.isEmpty()) {
                    System.out.println("No records found in the result set.");
                    break;
                }
    
                for (Record t : records3) {
                    try {
                        var node = t.get("t").asNode();

                        long likeCount = node.get("like_count").asLong();
                        String tweetLang = node.get("lang").asString();
                        long tweetId = node.get("tweet_id").asLong();
                        String text = node.get("text").asString();
                        long replyCount = node.get("reply_count").asLong();
                        long retweetCount = node.get("retweet_count").asLong();
    
                        if (tweetLang.equals(lang) && likeCount >= x && retweetCount >= y) {
                            hasValidTweet = true;
                            break;
                        }
                    } catch (Exception e) {
                        System.err.println("Error occurred in loop: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
    
                if (hasValidTweet) {
                    g_users.add(user);
                }
            }
        } catch (Exception e) {
            System.err.println("Error occurred while processing a record: " + e.getMessage());
            e.printStackTrace();
        }
        }
}

