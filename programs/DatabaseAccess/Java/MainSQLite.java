import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainSQLite {
    
    public static class User {
        private BigInteger user_id;
        private String username;
        private long followers_count;
        private long following_count;
        private long tweet_count;
        private int verified;
 
        public User(BigInteger user_id, String username, long followers_count, long following_count, long tweet_count, int verified) {
                this.user_id = user_id;
                this.username = username;
                this.followers_count = followers_count;
                this.following_count = following_count;
                this.tweet_count = tweet_count;
                this.verified = verified;
            }
    }

    public static class Tweet {
        private int tweet_id;
        private String text;
        private long like_count;
        private long retweet_count;
        private long reply_count;
        private String lang;
        private BigInteger user_id;
    
    public Tweet(int tweet_id, String text, long like_count, long retweet_count, long reply_count, String lang, BigInteger user_id) {
            this.tweet_id = tweet_id;
            this.text = text;
            this.like_count = like_count;
            this.retweet_count = retweet_count;
            this.reply_count = reply_count;
            this.lang = lang;
            this.user_id = user_id;
        }
    }
         
    private static List<User> g_users = new ArrayList<>();
    private static List<User> g_users_temp = new ArrayList<>();
    
    private static final String REQUEST1_SQL = "SELECT DISTINCT u.* FROM User u, Tweet t"+
                                                " WHERE t.like_count > ?"+
                                                    " AND t.retweet_count > ?"+
                                                    " AND t.lang = ?"+
                                                    " AND t.user_id = u.user_id";
    
    
    private static final String REQUEST2_SQL = "SELECT DISTINCT u.* FROM User u, Tweet t"+
                                                " WHERE t.user_id = u.user_id"+
                                                    " AND t.lang = ?";

    private static final String REQUEST3_SQL = "SELECT distinct t.* from Tweet t "+
                                                " WHERE t.user_id = ?";



    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: database [single|multiple]");
            return;
        }

        String requestType = args[1].toLowerCase();
        if (!requestType.equals("single") && !requestType.equals("multiple") && !requestType.equals("tweet")) {
            System.out.println("Invalid request type. Usage: [single|multiple]");
            return;
        }    

        try(Connection connection = DriverManager.getConnection("jdbc:sqlite:"+ args[0]);){
            
            if (requestType.equals("single")) {
                single_request(connection, 10000, 1000, "fr");
            } 
            else if (requestType.equals("multiple")) {
                multiple_requests(connection, 10000, 1000, "fr");
            }
           
            System.out.println("Retrieved: " + g_users.size());
        } catch(SQLException e) {
          e.printStackTrace(System.err);
        }
    }
    
    public static void single_request(Connection connection, long x, long y, String lang) {
        try (PreparedStatement statement = connection.prepareStatement(REQUEST1_SQL)) {
            statement.setLong(1, x);
            statement.setLong(2, y);
            statement.setString(3, lang);

            ResultSet rs = statement.executeQuery();

            while (rs.next()) {

                String userIdString = rs.getString("user_id");
                BigInteger userId = new BigInteger(userIdString); 
                String username = rs.getString("username");
                long followersCount = rs.getLong("followers_count");
                long followingCount = rs.getLong("following_count");
                long tweetCount = rs.getLong("tweet_count");
                int verified = rs.getInt("verified");
                     
                User user = new User(userId, username, followersCount, followingCount, tweetCount, verified);
                g_users.add(user);
            }
            
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }
    }

    public static void multiple_requests(Connection connection, long x, long y, String lang){
        try (PreparedStatement statement = connection.prepareStatement(REQUEST2_SQL)) {
            statement.setString(1, lang);

            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                String userIdString = rs.getString("user_id");
                BigInteger userId = new BigInteger(userIdString); 
                String username = rs.getString("username");
                long followersCount = rs.getLong("followers_count");
                long followingCount = rs.getLong("following_count");
                long tweetCount = rs.getLong("tweet_count");
                int verified = rs.getInt("verified");
                
                User user = new User(userId, username, followersCount, followingCount, tweetCount, verified);
                g_users_temp.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace(System.err);
        }

        List<User> usersToAdd = new ArrayList<>();
        for (User user : g_users_temp) {
            try (PreparedStatement statement2 = connection.prepareStatement(REQUEST3_SQL)) {
                statement2.setString(1, user.user_id.toString());
                ResultSet rs2 = statement2.executeQuery();
        
                boolean hasValidTweet = false;
        
                while (rs2.next()) {
                    try {
                        BigInteger tweet_id = new BigInteger(rs2.getString("tweet_id"));
                        String text = rs2.getString("text");
                        long likeCount = rs2.getLong("like_count");
                        long retweetCount = rs2.getLong("retweet_count");
                        long reply_count = rs2.getLong("reply_count");
                        String tweetLang = rs2.getString("lang");
                        BigInteger user_id = new BigInteger(rs2.getString("user_id"));
        
                        if (tweetLang.equals(lang) && likeCount >= x && retweetCount >= y) {
                            hasValidTweet = true;
                            break;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace(System.err);
                    }
                }
                if (hasValidTweet) {
                    usersToAdd.add(user); 
                }
            } catch (SQLException e) {
                e.printStackTrace(System.err);
            }
        }
        g_users.addAll(usersToAdd);
    }
}
    
