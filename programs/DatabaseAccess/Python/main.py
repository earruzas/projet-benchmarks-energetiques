from dataclasses import dataclass
from typing import List
import mariadb
import sys

@dataclass
class User:
    user_id: int
    username: str
    followers_count: int
    following_count: int
    tweet_count: int
    verified: int

@dataclass
class Tweet:
    tweet_id: int
    text: str
    like_count: int
    retweet_count: int
    reply_count: int
    lang: str
    user_id: int

g_users: List[User] = []

request1_sql = '''
SELECT DISTINCT u.* FROM User u, Tweet t
    WHERE t.like_count > ? AND
    t.retweet_count > ? AND
    t.lang = ? AND
    t.user_id = u.user_id
'''

def single_request(connection: mariadb.Connection, x: int, y: int, lang: str):
    cursor = connection.cursor()

    cursor.execute(request1_sql, (x, y, lang))

    for line in cursor:
        g_users.append(User(*line))

def multiple_requests(connection: mariadb.Connection, x: int, y: int, lang: str):
    cursor = connection.cursor()

    cursor.execute('SELECT DISTINCT u.* from User u, Tweet t where t.user_id = u.user_id and t.lang = ?', (lang,))

    users = []
    for line in cursor:
        users.append(User(*line))


    for user in users:
        cursor.execute('SELECT distinct t.* from Tweet t where ? = t.user_id', (user.user_id, ))
        for line in cursor:
            tweet = Tweet(*line)
            if tweet.lang == lang and tweet.like_count >= x and tweet.retweet_count >= y:
                g_users.append(user)
                break
        
def main():
    if len(sys.argv) < 2:
        print(f'Usage : script.py [single|multiple]')
        return

    connection = mariadb.connect(
        user="mysql",
        password="mysql",
        host="localhost",
        port=3306,
        database="tweetsdb"
    )
    if sys.argv[1].lower() == 'single':
        single_request(connection, 10000, 1000, 'fr')
    else:
        multiple_requests(connection, 10000, 1000, 'fr')

    print(f'Retrieved: {len(g_users)} users')


if __name__ == "__main__":
    main()