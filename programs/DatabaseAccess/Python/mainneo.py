from typing import List
import sys
from neo4j import GraphDatabase
from dataclasses import dataclass


uri = "bolt://127.0.0.1:7687" 
username = "neo4j" 
password = "tweetsdb"

driver = GraphDatabase.driver(uri, auth=(username, password))

@dataclass
class User:
    user_id: int
    username: str
    followers_count: int
    following_count: int
    tweet_count: int
    verified: int

@dataclass
class Tweet:
    tweet_id: int
    text: str
    like_count: int
    retweet_count: int
    reply_count: int
    lang: str
    user_id: int

g_users: List[User] = []
request1_neo = '''
MATCH (u:User)-[:POSTED]->(t:Tweet)
WHERE t.like_count > $likeCount AND
      t.retweet_count > $rtCount AND
      t.lang = $language 
RETURN DISTINCT u
'''
        
#'select distinct t.* from Tweet t where ? = t.user_id', (user.user_id, ))

request2_neo = '''
MATCH (u:User)-[:POSTED]->(t:Tweet)
WHERE t.user_id = u.user_id AND
      t.lang = $lang
RETURN DISTINCT u
''' 

request3_neo = '''
MATCH (u:User)-[:POSTED]->(t:Tweet)
WHERE t.user_id = $userid
RETURN DISTINCT t
'''

def single_request(tx, x: int, y: int, lang: str):
    result = tx.run(request1_neo, likeCount=x, rtCount=y, language=lang)

    for record in result:
        user_record = record['u']
        user = User(user_record['user_id'], user_record['username'], user_record['followers_count'], user_record['following_count'], user_record['tweet_count'], user_record['verified'])
        g_users.append(user)

def multiple_requests(tx, x: int, y: int, lang: str):
    result = tx.run(request2_neo, lang=lang)
    g_users_temp: List[User] = []
    for record in result:
        user_record = record['u']
        user = User(user_record['user_id'], user_record['username'], user_record['followers_count'], user_record['following_count'], user_record['tweet_count'], user_record['verified'])
        g_users_temp.append(user)
    
    for user in g_users_temp:
        result = tx.run(request3_neo, userid=user.user_id)
        for record in result:
            tweet_record = record['t']
            tweet = Tweet(tweet_record['tweet_id'], tweet_record['text'], tweet_record['like_count'], tweet_record['retweet_count'], tweet_record['reply_count'], tweet_record['lang'], tweet_record['user_id'])
            if tweet.lang == lang and tweet.like_count > x and tweet.retweet_count > y:
                g_users.append(user)

def main():
    if len(sys.argv) < 2:
        print(f'Usage : script.py [single|multiple]')
        return

    with GraphDatabase.driver(uri, auth=(username, password)) as driver:
        with driver.session(database="neo4j") as session:

            if sys.argv[1].lower() == 'single':
                session.execute_read(single_request, 10000, 1000, 'fr')    
            else:
                session.execute_read(multiple_requests, 10000, 1000, 'fr')

            print(f'Retrieved: {len(g_users)} users')

if __name__ == "__main__":
    main()
    
