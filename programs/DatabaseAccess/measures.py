import subprocess, time, csv, glob, os
from typing import Tuple

from utils import parse_joules_seconds
from utils import perf_command

def measure(command: str, mode: str) -> Tuple[float, float, float, float]:
    command = f'notfloc.py {command} {mode}'.split(' ')

    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    ret = process.wait()

    if ret != 0:
        print(' '.join(command))
        raise Exception("Ooops")

    data = process.stdout.read()
    out = parse_joules_seconds(data)
    print('Exec done: ', out)
    return out

def do_all_measures(out_dir: str):

    old = os.getcwd()
    os.chdir('programs/DatabaseAccess')

    commands = [
        ('./C/mariadb', './C/neo4j'),
        ('java -cp ~/jars/*:. ./Java/MainMariadb.java', 'java -cp ~/jars/*:. ./Java/MainNeo4j.java'),
        ('python3 ./Python/main.py', 'python3 ./Python/mainneo.py')]
    names = ('MariaDB', 'Neo4J')
    modes = ['single', 'multiple'] 

    csv_file = open(f'{out_dir}/measures_dbaccess.csv', 'w', newline='')
    writer = csv.writer(csv_file, delimiter=';')
    writer.writerow(['language', 'db', 'mode', 'cpu', 'ram', 'nic', 'sd', 'duration'])


    for commands, language in zip(commands, ('C', 'Java', 'Python')):
        for command, name in (zip(commands, names)):

            for mode in modes:
                for _ in range(5):
                    cpu, ram, nic, sd, duration = measure(command, mode)

                    writer.writerow([language, name, mode, f'{cpu}', f'{ram}', f'{nic}', f'{sd}, 'f'{duration}'])

        csv_file.close()

    os.chdir(old)


