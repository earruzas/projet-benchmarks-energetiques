import glob
import bz2
import json
import tempfile
import csv
from neo4j import GraphDatabase
from dataclasses import dataclass
from mariadb import Connection
import mariadb
def create_tables(connection: Connection):
    cur = connection.cursor()
    cur.execute("""
    CREATE TABLE User(
        user_id BIGINT UNSIGNED PRIMARY KEY,
        username varchar(100),
        followers_count INT,
        following_count INT,
        tweet_count INT,
        verified INT
    )
    """)

    cur.execute("""
    CREATE TABLE Tweet(
        tweet_id BIGINT UNSIGNED PRIMARY KEY,
        text varchar(300),
        like_count INT,
        retweet_count INT,
        reply_count INT,
        lang varchar(5),

        user_id BIGINT UNSIGNED,
        FOREIGN KEY(user_id) REFERENCES User(user_id)
    )
    """)

@dataclass
class User:
    user_id: int
    username: str
    followers_count: int
    following_count: int
    tweet_count: int
    verified: int

    def __eq__(self, __value: object) -> bool:
        return self.user_id == __value.user_id
    
    def __hash__(self) -> int:
        return self.user_id

@dataclass
class Tweet:
    tweet_id: int
    text: str
    like_count: int
    retweet_count: int
    reply_count: int
    lang: str
    user_id: int

    def __eq__(self, __value: object) -> bool:
        return self.tweet_id == __value.tweet_id
    
    def __hash__(self) -> int:
        return self.tweet_id

def parse_user(data):
    uid = int(data['id'])
    username = data['username']
    followers_count = data['public_metrics']['followers_count']
    following_count = data['public_metrics']['following_count']
    tweet_count = data['public_metrics']['tweet_count']
    verified = data['verified']

    return User(uid, username, followers_count, following_count, tweet_count, int(verified))


def parse_tweet(tweet):
    if 'referenced_tweets' in tweet.keys():
        return None

    lang = tweet['lang']
    tweet_id = int(tweet['id'])
    author_id = tweet['author_id']
    content = tweet['text']
    like_count = tweet['public_metrics']['like_count']
    retweet_count = tweet['public_metrics']['retweet_count']
    reply_count = tweet['public_metrics']['reply_count']

    return Tweet(tweet_id, content, like_count, retweet_count, reply_count, lang, author_id)


def insert_user_mariadb(user: User, connection: Connection):
    cur = connection.cursor()
    try:
        cur.execute(f"""
            INSERT INTO User values ({user.user_id}, '{user.username}', {user.followers_count}, {user.following_count}, {user.tweet_count}, {user.verified}) 
        """)
    except Exception as e:
        pass # Unique constraint failed on user_id
                
def insert_tweet_mariadb(t: Tweet, connection: Connection):
    cur = connection.cursor()
    t.text = t.text.replace("'", "")
    try:

        cur.execute(f"""
            INSERT INTO Tweet values ({t.tweet_id}, '{t.text}', {t.like_count}, {t.retweet_count}, {t.reply_count}, '{t.lang}', {t.user_id})
        """)
    except Exception as e:
        pass # Unique constraint failed on tweet_id
    
def insert_users_neo4j(session, csv_name: str):
    session.run(f"""
        LOAD CSV FROM 'file://{csv_name}' AS line
        CALL {{
            WITH line
            MERGE (u:User {{user_id: line[0]}})
            set u.username = line[1], u.followers_count = line[2], u.following_count = line[3], u.tweet_count = line[4], u.verified = line[5]
        }} IN TRANSACTIONS OF 1000 ROWS
    """)

def insert_tweets_neo4j(session, csv_name: str):
    # way better performance when creating nodes, and relationships after
    session.run(f"""
        LOAD CSV FROM 'file://{csv_name}' AS line
        CALL {{
            WITH line
            MERGE (t:Tweet {{tweet_id: line[0]}})
            set t.text = line[1], t.like_count = toInteger(line[2]), t.retweet_count = toInteger(line[3]), t.reply_count = toInteger(line[4]), t.lang = line[5], t.user_id = line[6]
        }} IN TRANSACTIONS OF 100 ROWS
    """)

    # Creating indexes (should improve performance)
    try:
        session.run('CREATE INDEX FOR (u: User) ON (u.user_id)')
        session.run('CREATE INDEX FOR (t: Tweet) ON (t.user_id)')
    except Exception as e:
        pass # occurs if indexes already exist


    # Creating relationships user/tweet, (maybe have to run it manually)
    session.run("""
        call apoc.periodic.iterate("
            match (t: Tweet)
            match (u: User {user_id: t.user_id})
            return u, t", 
        "merge (u)-[:POSTED]->(t)",  {batchSize: 1000})
    """)

# mariadb
connection = mariadb.connect(
    user="mysql",
    password="mysql",
    host="localhost",
    port=3306,
    database="tweetsdb"
)

# neo4j
uri = "bolt://127.0.0.1:7687" 
username = "neo4j" 
password = "tweetsdb"

#driver = GraphDatabase.driver(uri, auth=(username, password))
#session = driver.session()

try:
    create_tables(connection)
except Exception as e:
    print('Tables already exist')


files = glob.glob('tweet2023/*/*/*.json.bz2')

print(len(files))

users_g = set()
tweets_g = set()

for i, filename in enumerate(files):
    print(i, filename)
    f = bz2.open(filename)

    for line in f.readlines():
        parsed = json.loads(line)
        tweets = parsed['includes'].get('tweets', [])
        users = parsed['includes'].get('users', [])

        for raw_user in users:
            user = parse_user(raw_user)
            users_g.add(user)
        
        for raw_tweet in tweets:
            tweet = parse_tweet(raw_tweet)
            if tweet is None:
                continue
            
            tweets_g.add(tweet)

    f.close()

# Inserting / Creating users
print(f'Inserting {len(users_g)} users')

file = tempfile.NamedTemporaryFile('+w')
csv_writer = csv.writer(file)

for u in users_g:
    csv_writer.writerow(
        [u.user_id, u.username, u.followers_count, u.following_count, u.tweet_count, u.verified]
    )
    insert_user_mariadb(u, connection)

file.flush()
insert_users_neo4j(driver.session(), file.name)
file.close()

# Inserting / Creating tweets
print(f'Inserting {len(tweets_g)} tweets')

file = tempfile.NamedTemporaryFile('+w')
csv_writer = csv.writer(file)

for t in tweets_g:
    # todo escape tweet content, so no issue while creating node
    csv_writer.writerow(
        [t.tweet_id, 'yyy', t.like_count, t.retweet_count, t.reply_count, t.lang, t.user_id]
    ) 
    insert_tweet_mariadb(t, connection)


file.flush()
insert_tweets_neo4j(driver.session(), file.name)
file.close()


connection.commit()
connection.close()

driver.close()