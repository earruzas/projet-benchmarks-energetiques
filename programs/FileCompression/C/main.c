#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <zlib.h>
#include <fts.h>

int gzip_compress(const char* filename, const char* compression_level, unsigned int buffer_size)
{
    char* outfile = malloc(strlen(filename) + 1 + 3); // str + .gz + '\0';
    snprintf(outfile, strlen(filename) + 1 + 3, "%s.gz", filename);

    gzFile filein = gzopen(filename, "rb");
    gzFile fileout = gzopen(outfile, compression_level);

    char* buffer = malloc(buffer_size);
    int ret = 1;

    if (filein == NULL)
    {
        printf("Error opening file \"%s\"\n", filename);
        goto end;
    }

    if (fileout == NULL)
    {
        printf("Error opening outfile \"%s\"\n", outfile);
        goto end;
    }

    unsigned int n;
    do
    {
        n = gzread(filein, buffer, buffer_size);
        if (n > 0)
        {
            if (gzwrite(fileout, buffer, n) == 0)
            {
                printf("Error while writing out file (should not happen?)\n");
                goto end;
            }
        }
    } while (n > 0);

    ret = 0;
    end:
        free(buffer);
        gzclose(filein);
        gzclose(fileout);
    return ret;
}

unsigned int str_to_unsigned(const char* number)
{
    char* end;
    unsigned int out = strtoul(number, &end, 10);
    if (errno == ERANGE || *end != '\x00')
    {
        fprintf(stderr, "Got an invalid number");
        exit(1);
    }

    return out;
}

int main(int argc, const char* argv[])
{
    if (argc != 4)
    {
        printf("Usage: %s [directory] [compression level] [buffer size] \n", argv[0]);
        printf("\tExample: %s dirname 9 4096\n", argv[0]);
        return 1;
    }

    char* dir[2] = {argv[1], NULL};
    unsigned int compression_level = str_to_unsigned(argv[2]);
    unsigned int buffer_size = str_to_unsigned(argv[3]);

    if (compression_level < 1 ||compression_level > 9)
    {
        printf("Compression level must be between 1 and 9\n");
        return 1;
    }
    
    char* compression_level_str = malloc(3);
    snprintf(compression_level_str, 3, "w%.1d", compression_level);

    FTS* ftsp = fts_open(dir, FTS_NOCHDIR | FTS_NOSTAT | FTS_COMFOLLOW, NULL);
    if (ftsp == NULL)
    {
        printf("Error fts_open\n");
        return 1;
    }

    FTSENT* chp = fts_children(ftsp, 0);
    if (chp == NULL)
    {
        printf("Directory is empty\n");
        return 1;
    }

    FTSENT* p;
    while ((p = fts_read(ftsp)) != NULL)
    {
        if (p->fts_info != FTS_F)
            continue;

        printf("File : %s\n", p->fts_path);
        if (gzip_compress(p->fts_path, compression_level_str, buffer_size) != 0)
        {
            printf("It failed\n");
            return 1;
        }
    }

    fts_close(ftsp);
    free(compression_level_str);
    return 0;
}