import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

class Main {
    public static void main(String args[]) {
        if (args.length < 3) {
            System.out.println("Usage: Main.java [directory] [compression level] [buffer size]");
            System.out.println("\tExample: Main.java directory/ 9 4096");
            return;
        }

        File directory = new File(args[0]);
        if (! directory.isDirectory()) {
            System.err.println("Not a directory");
            return;
        }

        Integer compressionLevel = Integer.valueOf(args[1]);
        Integer bufferSize = Integer.valueOf(args[2]);

        for (File file : directory.listFiles()) {
            if (! file.isFile())
                continue;

            try {
                compress(file, compressionLevel, bufferSize);
            } catch (final IOException e) {
                System.err.println("Ooops");
                e.printStackTrace();
            }


            System.out.println(file.toPath().toString());
        }
    }

    public static void compress(File inFile, int compressionLevel, int bufferSize) throws IOException {
        File outFile = new File(inFile.toPath().toString() + ".gz");

        FileInputStream inputStream = new FileInputStream(inFile);
        CustomGZIPOutputStream outputStream = new CustomGZIPOutputStream(new FileOutputStream(outFile), compressionLevel);
    
        byte[] buffer = new byte[bufferSize];

        int len;

        while ((len = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, len);
        }

        inputStream.close();
        outputStream.close();

    }
}

// Simple class that inherits GZIPOutputStream
class CustomGZIPOutputStream extends GZIPOutputStream {
    CustomGZIPOutputStream(FileOutputStream stream, int compressionLevel) throws IOException {
        super(stream);
        this.def.setLevel(compressionLevel);
    }
}