import gzip
import os
import sys

def gzip_compress(file_path: str, compression_level: int, buffer_size: int) -> int:
    filein = open(file_path, 'rb')
    with gzip.open(file_path + '.gz', 'wb', compresslevel=compression_level) as fileout:
        while True:
            chunk = filein.read(buffer_size)
            if not chunk:
                break
            fileout.write(chunk)
    return 0

def main(dir_name: str, compression_level: int, buffer_size: int) -> None:
    if not 1 <= compression_level <= 9:
        print("Compression level must be between 1 and 9")
        return
    
    for root, _, files in os.walk(dir_name):
        for file in files:
            file_path = os.path.join(root, file)
            print("File :", file_path)
            if gzip_compress(file_path, compression_level, buffer_size) != 0:
                print("It failed")
                return
    return
    
# Dynamically enter arguments
if __name__ == '__main__':
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} [directory] [compression level] [buffer size]")
        print(f"Exemple: {sys.argv[0]} dirname 9 4096")
        sys.exit(1)

    main(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))