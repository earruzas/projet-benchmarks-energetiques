import subprocess, time, csv, glob, os
from typing import Tuple

from utils import parse_joules_seconds

def measure(buffsize: int, compression_level: int, command: str) -> Tuple[float, float, float, float]:
    command = f'notfloc.py {command} pictures/ {compression_level} {buffsize}'.split(' ')

    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    ret = process.wait()

    if ret != 0:
        raise Exception("Ooops")

    data = process.stdout.read()
    out = parse_joules_seconds(data)
    print('Exec done: ', out)
    return out

def do_all_measures(out_dir: str):

    old = os.getcwd()
    os.chdir('programs/FileCompression')

    commands = ['./C/main', 'java ./Java/Main.java', 'python3 ./Python/main.py']
    buffer_sizes = [65536, 262144, 1048576]
    compression_levels = [5, 7, 9]

    csv_file = open(f'{out_dir}/measure_filecompression.csv', 'w', newline='')
    writer = csv.writer(csv_file, delimiter=';')
    writer.writerow(['language', 'buffer-size', 'compression-level', 'cpu', 'ram', 'sd', 'compression', 'duration'])

    for command, name in zip(commands, ('C', 'Java', 'Python')):
        
        for size in buffer_sizes:
            for cl in compression_levels:
                for _ in range(5):
                    cpu, ram, nic, sd, duration = measure(size, cl, command)

                    avg_compression_ratio = 0
                    files = glob.glob('pictures/*.xml')
                    for file in files:
                        cfile = file + '.gz'
                        f_size = os.path.getsize(file)
                        cf_size = os.path.getsize(cfile)
                        avg_compression_ratio += f_size / cf_size 
                    avg_compression_ratio /= len(files)

                    writer.writerow([name, f'{size}', f'{cl}', f'{cpu}', f'{ram}', f'{sd}', f'{avg_compression_ratio}', f'{duration}'])

                    # Remove compressed files
                    files = glob.glob('pictures/*.xml.gz')
                    for f in files:
                        os.remove(f)
    csv_file.close()

    os.chdir(old)


