#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netinet/in.h> 
#include <errno.h>
#include <string.h>

struct initial_information {
    char filename[32];
    size_t filesize;
    size_t block_size;
};

int create_socket(unsigned int port)
{
    int fdsocket = socket(AF_INET, SOCK_STREAM, 0);
    if (fdsocket == -1)
        return -1;

    int opt = 1;
    if (setsockopt(fdsocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) != 0)
        return -1;

    return fdsocket;
}

unsigned int str_to_unsigned(const char* number)
{
    char* end;
    unsigned int out = strtoul(number, &end, 10);
    if (errno == ERANGE || *end != '\x00')
    {
        fprintf(stderr, "Got an invalid number");
        exit(1);
    }

    return out;
}

// force reading exactly nbytes from fd
int force_read(int fd, char* buffer, size_t nbytes)
{
    size_t rread = 0;
    size_t remaining = nbytes;
    while (remaining != 0)
    {
        size_t value = read(fd, buffer, remaining);
        remaining -= value;
        buffer = buffer + value;
    }
}

int main(int argc, const char* argv[])
{
    if (argc != 3)
    {
        printf("Usage: %s [port] [filename]\n", argv[0]);
        printf("\tExample: %s 4444 out\n", argv[0]);
        return 1;
    }

    unsigned int port = str_to_unsigned(argv[1]);
    const char* out_file = argv[2];

    int fdsocket = create_socket(port);

    struct sockaddr_in address = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = inet_addr("127.0.0.1"),
        .sin_port = htons(port)
    };

    if (connect(fdsocket, (struct sockaddr*)&address, sizeof(address)) != 0)
    {
        fprintf(stderr, "Error : connection to server\n");
        return 1;
    }

    struct initial_information info;
    read(fdsocket, &info, sizeof(struct initial_information));

    printf("Receiving file %s (%ld bytes) with block size : %ld\n", info.filename, info.filesize, info.block_size);
    
    char* buffer = malloc(info.block_size + 8);
    int out = open(out_file, O_WRONLY | O_CREAT, 0644);
    if (out == -1)
    {
        fprintf(stderr, "Error : opening file for output\n");
        return 1;
    }

    uint64_t no_blocks = info.filesize / info.block_size;

    for (uint64_t i = 0; i < no_blocks; i++)
    {
        force_read(fdsocket, buffer, info.block_size + 8);
        write(fdsocket, buffer, 8); // ACK (for sync)
        uint64_t block_id = *(uint64_t*)(buffer);
        write(out, buffer+8, info.block_size);
    }

    free(buffer);
    close(out);
    close(fdsocket);

    return 0;
}