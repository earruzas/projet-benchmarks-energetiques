import os, csv

from typing import Optional, Tuple

import subprocess, time


from utils import parse_joules_seconds


def measure(block_size: int, thread_no: Optional[int], server_command: str, client_command: str) -> Tuple[float, float, float, float]:
    if thread_no:
        server_command = f'notfloc.py {server_command} randomfile 4444 {block_size} {thread_no}'.split(' ')
        client_command = f'notfloc.py {client_command} 4444 {thread_no} out'.split(' ')
    else:
        server_command = f'notfloc.py {server_command} randomfile 4444 {block_size}'.split(' ')
        client_command = f'notfloc.py {client_command} 4444 out'.split(' ')

    server_proc = subprocess.Popen(server_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    time.sleep(0.5)
    client_proc = subprocess.Popen(client_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    ret = server_proc.wait()
    if ret != 0:
        print(server_proc.stdout.read())
        raise Exception("Ooops")

    data = server_proc.stdout.read()
    out = parse_joules_seconds(data)
    print('Exec done: ', out)

    os.remove('out')

    return out



def do_measures_C(out_dir: str):
    block_sizes = [4096, 65536, 1048576]
    thread_number = [3, 5, 10]

    os.makedirs(out_dir, exist_ok=True)
    old = os.getcwd()

    os.chdir('programs/FileTransfert/C/')

    file = open(f'{out_dir}/measures_filetransfert_c.csv', 'w', newline='')
    writer = csv.writer(file, delimiter=';')
    writer.writerow(['mode', 'block-size', 'thread_number', 'cpu', 'ram', 'nic', 'sd', 'duration'])

    for size in block_sizes:
        for _ in range(5):
            print(f'Executing with parameters : {size}')
            cpu, ram, nic, sd, duration = measure(size, None, './server', './client')
            writer.writerow(['monothread', '0', f'{size}', f'{cpu}', f'{ram}', f'{nic}', f'{sd}', f'{duration}'])

    for size in block_sizes:
        for th_number in thread_number:
            for _ in range(5):
                print(f'Executing thread with parameters : {size} {th_number}')
                cpu, ram, nic, sd, duration = measure(size, th_number, './thread/server', './thread/client')
                writer.writerow(['thread', f'{th_number}', f'{size}', f'{cpu}', f'{ram}', f'{nic}', f'{sd}', f'{duration}'])

    for size in block_sizes:
        for th_number in thread_number:
            for _ in range(5):
                print(f'Executing fork with parameters : {size} {th_number}')
                cpu, ram, nic, sd, duration = measure(size, th_number, './fork/server', './fork/client')
                writer.writerow(['fork', f'{th_number}', f'{size}', f'{cpu}', f'{ram}', f'{nic}', f'{sd}', f'{duration}'])

    # go back to old dir
    os.chdir(old)
