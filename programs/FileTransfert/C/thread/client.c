#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netinet/in.h> 
#include <errno.h>
#include <string.h>
#include <pthread.h>

struct initial_information {
    char filename[32];
    size_t filesize;
    size_t block_size;
};

struct {
    int file_fd;
    int socket_fd;
    uint64_t block_size;
    uint64_t no_blocks;
    uint64_t expected;
    uint64_t count;
    pthread_mutex_t mutex;
} g_state;

int create_socket(unsigned int port)
{
    int fdsocket = socket(AF_INET, SOCK_STREAM, 0);
    if (fdsocket == -1)
        return -1;

    int opt = 1;
    if (setsockopt(fdsocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) != 0)
        return -1;

    return fdsocket;
}

unsigned int str_to_unsigned(const char* number)
{
    char* end;
    unsigned int out = strtoul(number, &end, 10);
    if (errno == ERANGE || *end != '\x00')
    {
        fprintf(stderr, "Got an invalid number");
        exit(1);
    }

    return out;
}

// force reading exactly nbytes from fd
int force_read(int fd, char* buffer, size_t nbytes)
{
    size_t rread = 0;
    size_t remaining = nbytes;
    while (remaining != 0)
    {
        size_t value = read(fd, buffer, remaining);
        remaining -= value;
        buffer = buffer + value;
    }
}

void* thread_job(void* none)
{
    char* buffer = malloc(g_state.block_size + 8);
    for (;;)
    {
        uint64_t block_id = 0;
        pthread_mutex_lock(&g_state.mutex);
        {
            if (g_state.count >= g_state.no_blocks)
            {
                pthread_mutex_unlock(&g_state.mutex);
                break;
            }

            force_read(g_state.socket_fd, buffer, g_state.block_size + 8);
            block_id = *(uint64_t*)(buffer);
            g_state.count += 1;
        }
        pthread_mutex_unlock(&g_state.mutex);
        
        while (block_id != g_state.expected)
            {}

        pthread_mutex_lock(&g_state.mutex);
        {
            write(g_state.file_fd, buffer + 8, g_state.block_size);
            g_state.expected += 1;
        }
        pthread_mutex_unlock(&g_state.mutex);


    }
    free(buffer);
    pthread_exit(0);
}

int main(int argc, const char* argv[])
{
    if (argc != 4)
    {
        printf("Usage: %s [port] [number of thread] [filename]\n", argv[0]);
        printf("\tExample: %s 4444 10 out\n", argv[0]);
        return 1;
    }

    unsigned int port = str_to_unsigned(argv[1]);
    unsigned int no_thread = str_to_unsigned(argv[2]);
    const char* out_file = argv[3];

    int fdsocket = create_socket(port);

    struct sockaddr_in address = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = inet_addr("127.0.0.1"),
        .sin_port = htons(port)
    };

    if (connect(fdsocket, (struct sockaddr*)&address, sizeof(address)) != 0)
    {
        fprintf(stderr, "Error : connection to server\n");
        return 1;
    }

    struct initial_information info;
    read(fdsocket, &info, sizeof(struct initial_information));

    printf("Receiving file %s (%ld bytes) with block size : %ld\n", info.filename, info.filesize, info.block_size);
    
    int out = open(out_file, O_WRONLY | O_CREAT, 0644);
    if (out == -1)
    {
        fprintf(stderr, "Error : opening file for output\n");
        return 1;
    }

    uint64_t no_blocks = info.filesize / info.block_size;

    g_state.block_size = info.block_size;
    g_state.file_fd = out;
    g_state.socket_fd = fdsocket;
    g_state.no_blocks = no_blocks;
    g_state.expected = 0;
    g_state.count = 0;

    pthread_mutex_init(&g_state.mutex, NULL);

    pthread_t threads[no_thread];

    for (unsigned i = 0; i < no_thread; i++)
    {
        pthread_create(&threads[i], NULL, thread_job, NULL);
    }

    for (unsigned i = 0; i < no_thread; i++)
    {
        pthread_join(threads[i], NULL);
    }

    write(fdsocket, "AAAAAAAA", 8);

    close(out);
    close(fdsocket);

    return 0;
}