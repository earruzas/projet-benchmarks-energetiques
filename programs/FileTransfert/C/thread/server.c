#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netinet/in.h> 
#include <errno.h>
#include <string.h>
#include <pthread.h>

struct initial_information {
    char filename[32];
    size_t filesize;
    size_t block_size;
};

struct {
    int file_fd;
    int client_fd;
    uint64_t block_size;
    uint64_t no_blocks;
    uint64_t current_block;
    pthread_mutex_t mutex;
} g_state;

int create_socket(unsigned int port)
{
    int fdsocket = socket(AF_INET, SOCK_STREAM, 0);
    if (fdsocket == -1)
        return -1;

    int opt = 1;
    if (setsockopt(fdsocket, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) != 0)
        return -1;

    struct sockaddr_in address = {
        .sin_family = AF_INET,
        .sin_addr.s_addr = INADDR_ANY,
        .sin_port = htons(port)
    };

    if (bind(fdsocket, (struct sockaddr*)&address, sizeof(address)) != 0)
        return -1;

    if (listen(fdsocket, 5) != 0)
        return -1;

    return fdsocket;
}

unsigned int str_to_unsigned(const char* number)
{
    char* end;
    unsigned int out = strtoul(number, &end, 10);
    if (errno == ERANGE || *end != '\x00')
    {
        fprintf(stderr, "Got an invalid number");
        exit(1);
    }

    return out;
}

int force_read(int fd, char* buffer, size_t nbytes)
{
    size_t rread = 0;
    size_t remaining = nbytes;
    while (remaining != 0)
    {
        size_t value = read(fd, buffer, remaining);
        remaining -= value;
        buffer = buffer + value;
    }
}

void* thread_job(void* none)
{
    char* buffer = malloc(g_state.block_size + 8);
    for (;;)
    {
        pthread_mutex_lock(&g_state.mutex);
        {
            uint64_t i = g_state.current_block;
            if (i >= g_state.no_blocks)
            {
                pthread_mutex_unlock(&g_state.mutex);
                break;
            }

            g_state.current_block += 1;

            *(uint64_t*)(buffer) = i;
            read(g_state.file_fd, buffer + 8, g_state.block_size);

            write(g_state.client_fd, buffer, g_state.block_size + 8);

        }
        pthread_mutex_unlock(&g_state.mutex);
    }

    free(buffer);
    pthread_exit(0);
}

int main(int argc, const char* argv[])
{
    if (argc != 5) {
        printf("Usage: %s [filename] [port] [block_size] [number of thread]\n", argv[0]);
        printf("\tExample: %s 4444 file.pdf 4096 10\n", argv[0]);
        return 1;
    }

    const char* file_path = argv[1];
    unsigned int port = str_to_unsigned(argv[2]);
    unsigned int block_size = str_to_unsigned(argv[3]);
    unsigned int no_thread = str_to_unsigned(argv[4]);

    int fdsocket = create_socket(port);

    struct stat file_stat;

    if (stat(file_path, &file_stat) != 0)
    {
        fprintf(stderr, "Error : couldn't stat file : \"%s\"\n", argv[1]);
        return 1;
    }

    if (file_stat.st_size % block_size != 0)
    {
        fprintf(stderr, "Error : file size is not a multiple of block size\n");
        return 1;
    }

    int fdfile = open(file_path, O_RDONLY);
    if (fdfile == -1)
    {
        fprintf(stderr, "Error : opening the file : \"%s\"\n", file_path);
        return 1;
    }

    struct sockaddr_in client_address;
    unsigned int len = sizeof(client_address);

    int client_socket = accept(fdsocket, (struct sockaddr*)&client_address, &len);
    if (client_socket == -1)
    {
        fprintf(stderr, "Error : client socket\n");
        return 1;
    }

    struct initial_information info = {
        .block_size = block_size,
        .filesize = file_stat.st_size
    };
    strncpy(info.filename, file_path, 32);

    write(client_socket, &info, sizeof(struct initial_information));

    char* buffer = malloc(block_size + sizeof(uint64_t));

    uint64_t no_blocks = file_stat.st_size / block_size ;

    printf("Sending file \"%s\" (%ld bytes) with block size : %d\n", info.filename, info.filesize, block_size);

    g_state.block_size = block_size;
    g_state.client_fd = client_socket;
    g_state.file_fd = fdfile;
    g_state.no_blocks = no_blocks;
    g_state.current_block = 0;

    pthread_mutex_init(&g_state.mutex, NULL);

    pthread_t threads[no_thread];

    for (unsigned i = 0; i < no_thread; i++)
    {
        pthread_create(&threads[i], NULL, thread_job, NULL);
    }

    for (unsigned i = 0; i < no_thread; i++)
    {
        pthread_join(threads[i], NULL);
    }

    read(fdsocket, buffer, 8);
    
    free(buffer);
    close(fdsocket);
    close(fdfile);
    close(client_socket);

    return 0;
}