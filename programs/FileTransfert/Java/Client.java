import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.io.File;
import java.io.FileOutputStream;


class ClientApp {
    public static void main(String args[]) {
        if (args.length < 2) {
            System.out.println("Usage: Client.java [port] [outfile]");
            System.out.println("\tExample: %s 4444 out");
            return;
        }

        int port = Integer.valueOf(args[0]);

        FileReceiver fileReceiver = new FileReceiver(port, args[1]);

        try {
            fileReceiver.initialization();
            fileReceiver.receiveFile();
        } catch (final Exception e) {
            System.out.println("Error : ");
            System.out.println(e.getMessage());
        }
    }
}


class FileReceiver {

    private final int m_port;
    private final String m_filename;
    long m_fileSize;
    long m_blockSize;

    private Socket m_socket;
    private File m_file;

    public FileReceiver(int port, String filename) {
        m_filename = filename;
        m_port = port;
        m_file = new File(m_filename);

        try {
            m_socket = new Socket("127.0.0.1", m_port);
        } catch (Exception e) {

        }
    }

    long bytesToLong(byte[] buffer, int at) {
        long value = 0;

        for (int i = 0; i < 8; i++) {
            value += (((long)buffer[at+i]) & 255) << (i * 8);
        }

        return value;
    }

    public void initialization() throws IOException {
        byte[] info = new byte[48];

        m_socket.getInputStream().read(info, 0, 48);

        m_fileSize = bytesToLong(info, 32);
        m_blockSize = bytesToLong(info, 40);
        String fileName = new String(info, StandardCharsets.US_ASCII);

        System.out.println(String.format(
            "Receiving file \"%s\" (%d bytes) with block size : %d", fileName, m_fileSize, m_blockSize
        ));
    }

    public void receiveFile() throws IOException {


        byte[] data = new byte[(int)m_blockSize+8];
        long no_blocks = m_fileSize / m_blockSize;

        FileOutputStream fileOut = new FileOutputStream(m_file);
        InputStream in = m_socket.getInputStream();

        for (long i = 0; i < no_blocks; i++) {
            
            in.readNBytes(data, 0, (int)m_blockSize+8);
            // long block_id = bytesToLong(data, 0);
            fileOut.write(data, 8, (int)m_blockSize);
        }

        // SENDING ACK to server
        m_socket.getOutputStream().write(new byte[]{0x41, 0x43, 0x4b, 0x00});

        fileOut.close();
        in.close();
        m_socket.close();
    }
}