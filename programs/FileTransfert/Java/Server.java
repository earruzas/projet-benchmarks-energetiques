import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import java.util.Arrays;
import java.io.File;
import java.io.FileInputStream;


class ServerApp {
    public static void main(String args[]) {

        if (args.length < 3) {
            System.out.println("Usage: Server.java [port] [filename] [block_size]");
            System.out.println("\tExample: Server.java 4444 file 4096");
            return;
        }

        int port = Integer.valueOf(args[0]);
        int blockSize = Integer.valueOf(args[2]);

        try {
            FileSender fileSender = new FileSender(args[1], port, blockSize);
            fileSender.initialization();
            fileSender.transfert();
        } catch (Exception e) {
            System.out.println("error : ");
            System.out.println(e.getMessage());
        }
    }   
}

class FileSender {

    private final String m_fileName;
    private final int m_port;
    private final int m_blockSize;
    private File m_file;
    private ServerSocket m_serverSocket = null;
    private Socket m_clientSocket = null;

    public FileSender(String filename, int port, int blockSize) throws Exception {
        m_fileName = filename;
        m_port = port;
        m_blockSize = blockSize;
        m_file = new File(m_fileName);

        if (!m_file.exists() || !m_file.canRead()) {
            throw new Exception("Error : can't open file");
        }
    
        try {
            m_serverSocket = new ServerSocket(m_port);
        } catch (IOException e) {

        }
    }

    private void longToUint64 (byte[] buffer, long value, int at) {
        for (int i = 0; i < 8; i++)
            buffer[i+at] = (byte)(value >> (i * 8) & 0xff);
    }

    private void sendInitialInformation() throws IOException {
        byte[] info = new byte[48];
        Arrays.fill(info, (byte)0);

        for (int i = 0; i < m_fileName.length() && i < 31; i++) { // Null terminated
            info[i] = (byte)m_fileName.charAt(i);
        }

        longToUint64(info, m_file.length(), 32);
        longToUint64(info, m_blockSize, 40);
        
        m_clientSocket.getOutputStream().write(info, 0, 48);
    }

    public void initialization() throws IOException {
        m_clientSocket = m_serverSocket.accept();

        System.out.println(String.format(
            "Sending file \"%s\" (%d bytes) with block size %d", m_fileName, m_file.length(), m_blockSize
        ));
        
        sendInitialInformation();
    }

    public void transfert() throws IOException {
        long no_blocks = m_file.length() / m_blockSize;
        FileInputStream fileData = new FileInputStream(m_file);

        OutputStream outputStream = m_clientSocket.getOutputStream();

        byte[] data = new byte[m_blockSize+8];
        for (long i = 0; i < no_blocks; i++) {
            longToUint64(data, i, 0);
            fileData.readNBytes(data, 8, m_blockSize);
            outputStream.write(data);
            outputStream.flush();
        }

        m_clientSocket.getInputStream().readNBytes(4); // waiting for the ack

        fileData.close();
        m_clientSocket.close();
        m_serverSocket.close();
    }
}