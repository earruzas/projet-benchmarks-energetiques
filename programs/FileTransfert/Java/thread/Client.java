import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.io.File;
import java.io.FileOutputStream;


class ClientApp {
    public static void main(String args[]) {
        if (args.length < 3) {
            System.out.println("Usage: Client.java [port] [outfile] [thread_number]");
            System.out.println("\tExample: %s 4444 out 10");
            return;
        }

        int port = Integer.valueOf(args[0]);
        int threadNumber = Integer.valueOf(args[2]);

        FileReceiver fileReceiver = new FileReceiver(port, args[1], threadNumber);

        try {
            fileReceiver.initialization();
            fileReceiver.receiveFile();
        } catch (final Exception e) {
            System.out.print("Error : ");
            System.out.println(e.getMessage());
        }
    }
}


class FileReceiver {

    private final int m_port;
    private final String m_filename;
    private final int m_threadNumber;
    private FileOutputStream m_fileOut;

    long m_fileSize;
    long m_blockSize;
    long m_blockReceived;
    long m_expectedBlock;

    ArrayList<byte[]> m_waitList;

    private Socket m_socket;
    private File m_file;

    public FileReceiver(int port, String filename, int threadNumber) {
        m_filename = filename;
        m_port = port;
        m_threadNumber = threadNumber;
        m_file = new File(m_filename);
        m_blockReceived = 0;
        m_expectedBlock = 0;
        m_waitList = new ArrayList<>();

        try {
            m_fileOut = new FileOutputStream(m_file);
            m_socket = new Socket("127.0.0.1", m_port);
        } catch (Exception e) {

        }
    }

    long bytesToLong(byte[] buffer, int at) {
        long value = 0;

        for (int i = 0; i < 8; i++) {
            value += (((long)buffer[at+i]) & 255) << (i * 8);
        }

        return value;
    }

    public void initialization() throws IOException {
        byte[] info = new byte[48];

        m_socket.getInputStream().read(info, 0, 48);

        m_fileSize = bytesToLong(info, 32);
        m_blockSize = bytesToLong(info, 40);
        String fileName = new String(info, StandardCharsets.US_ASCII);

        System.out.println(String.format(
            "Receiving file \"%s\" (%d bytes) with block size : %d", fileName, m_fileSize, m_blockSize
        ));
    }

    public synchronized void checkList() throws IOException {
        for (int j = 0; j < m_waitList.size(); j++) {
            byte[] b = m_waitList.get(j);
            long bId = bytesToLong(b, 0);
            if (bId == m_expectedBlock) {
                m_fileOut.write(b, 8, (int)m_blockSize);
                m_waitList.remove(j);
                j -= 1;
                m_expectedBlock += 1;
            }
        }
    }

    public void receiveFile() throws IOException {
        long no_blocks = m_fileSize / m_blockSize;

        InputStream in = m_socket.getInputStream();

        Thread[] threads = new Thread[m_threadNumber];

        for (int i = 0; i < m_threadNumber; i++) {
            Thread t = new Thread(() -> {
                try {
                    for (;;) {
                        byte[] data = new byte[(int)m_blockSize+8];

                        synchronized(this) {
                            if (m_blockReceived >= no_blocks)
                                break;
                            in.readNBytes(data, 0, (int)m_blockSize+8);                            
                            m_blockReceived += 1;
                        }

                        long blockId = bytesToLong(data, 0);
                        
                        synchronized(this) {

                            if (blockId == m_expectedBlock) {
                                m_fileOut.write(data, 8, (int)m_blockSize);
                                m_expectedBlock += 1;
                                checkList();
                            
                            } else {
                                m_waitList.add(data);
                                checkList();
                            }
                        }
                    }
                } catch (final Exception e) {

                }
            });
            t.start();
            threads[i] = t;
        }

        for (int i = 0; i < m_threadNumber; i++) {
            try {
                threads[i].join();
            } catch (Exception e) {}
        }

        // Writes all remaining blocks from list
        while (! m_waitList.isEmpty()) {
            checkList();
        }

        // SENDING ACK to server
        m_socket.getOutputStream().write(new byte[]{0x41, 0x43, 0x4b, 0x00});

        m_fileOut.close();
        m_socket.close();
    }
}