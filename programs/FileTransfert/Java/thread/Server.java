import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import java.util.Arrays;
import java.io.File;
import java.io.FileInputStream;


class ServerApp {
    public static void main(String args[]) {

        if (args.length < 4) {
            System.out.println("Usage: Server.java [port] [filename] [block_size] [no_thread]");
            System.out.println("\tExample: Server.java 4444 file 4096 10");
            return;
        }

        int port = Integer.valueOf(args[0]);
        int blockSize = Integer.valueOf(args[2]);
        int threadNumber = Integer.valueOf(args[3]);

        try {
            FileSender fileSender = new FileSender(args[1], port, blockSize, threadNumber);
            fileSender.initialization();
            fileSender.transfert();
            fileSender.end();
        } catch (Exception e) {
            System.out.println("error : ");
            System.out.println(e.getMessage());
        }
    }   
}

class FileSender {

    private final String m_fileName;
    private final int m_port;
    private final int m_blockSize;
    private final int m_threadNumber;
    private long m_blockSent = 0;
    private File m_file;
    private FileInputStream m_fileData;
    private ServerSocket m_serverSocket = null;
    private Socket m_clientSocket = null;

    public FileSender(String filename, int port, int blockSize, int threadNumber) throws Exception {
        m_fileName = filename;
        m_port = port;
        m_blockSize = blockSize;
        m_threadNumber = threadNumber;
        m_file = new File(m_fileName);

        if (!m_file.exists() || !m_file.canRead()) {
            throw new Exception("Error : can't open file");
        }

        m_fileData = new FileInputStream(m_file);

        try {
            m_serverSocket = new ServerSocket(m_port);
        } catch (IOException e) {

        }
    }

    private void longToUint64 (byte[] buffer, long value, int at) {
        for (int i = 0; i < 8; i++)
            buffer[i+at] = (byte)(value >> (i * 8) & 0xff);
    }

    private void sendInitialInformation() throws IOException {
        byte[] info = new byte[48];
        Arrays.fill(info, (byte)0);

        for (int i = 0; i < m_fileName.length() && i < 31; i++) { // Null terminated
            info[i] = (byte)m_fileName.charAt(i);
        }

        longToUint64(info, m_file.length(), 32);
        longToUint64(info, m_blockSize, 40);
        
        m_clientSocket.getOutputStream().write(info, 0, 48);
    }

    public void initialization() throws IOException {
        m_clientSocket = m_serverSocket.accept();

        System.out.println(String.format(
            "Sending file \"%s\" (%d bytes) with block size %d", m_fileName, m_file.length(), m_blockSize
        ));
        
        sendInitialInformation();
    }

    public void transfert() throws IOException {
        long no_blocks = m_file.length() / m_blockSize;

        OutputStream outputStream = m_clientSocket.getOutputStream();
        
        Thread[] threads = new Thread[m_threadNumber];

        for (int i = 0; i < m_threadNumber; i++) {
            Thread t = new Thread(() -> {
                try {
                    for (;;) {
                        byte[] data = new byte[(int)m_blockSize+8];

                        synchronized (this) {
                            if (m_blockSent >= no_blocks)
                                break;
    
                            m_fileData.readNBytes(data, 8, m_blockSize);
                            longToUint64(data, m_blockSent, 0);
                            m_blockSent += 1;
            
                            outputStream.write(data);
                            outputStream.flush();    
                        }
                    }
                } catch (final IOException e) {
                    System.out.print("Thread failed with exception : ");
                    System.out.println(e.getMessage());
                }
    
            });
            t.start();
            threads[i] = t;
        }

        for (int i = 0; i < m_threadNumber; i++) {
            try {
                threads[i].join();
            } catch (Exception e) {}
        }
    }

    public void end() throws IOException {
        m_clientSocket.getInputStream().readNBytes(4); // waiting for the ack

        m_fileData.close();
        m_clientSocket.close();
        m_serverSocket.close();
    }
}