import socket
import sys
import struct

def force_read(size: int, connection : socket) -> bytes:
    data = b''
    while len(data) != size:
        data += connection.recv(size - len(data))
    return data

# Receive files chunks by chunks 
def receive_file(connection: socket.socket, file_name: str, file_size: int, chunk_size: int):
    
    no_block = file_size // chunk_size
    file = open(file_name, 'wb')

    for _ in range(no_block):
        chunk_id = int.from_bytes(connection.recv(8), 'little')
        data = force_read(chunk_size, connection)
        file.write(data)
   
    # Send ack
    connection.send(b'ACK\x00')
    file.close()

# Main Client
def main(port: int, outfile: str):
    host = socket.gethostname()  

    client = socket.socket()  
    client.connect((host, port)) 
    
    # Receive basic informations
    info = client.recv(48)

    file_name = info[:32].decode()
    file_size = struct.unpack('<Q', info[32:40])[0]
    chunk_size = struct.unpack('<Q', info[40:48])[0]

    print(f"Receiving file: {file_name} ({file_size} bytes), with chunk size: {chunk_size}")
    
    receive_file(client, outfile, file_size, chunk_size)
    
    client.close()  

# Dynamically enter arguments
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print(f"Usage: {sys.argv[0]} [port] [filename]")
        print(f"Exemple: {sys.argv[0]} 4000 random_pic.jpg")
        sys.exit(1)

    main(int(sys.argv[1]), sys.argv[2])
