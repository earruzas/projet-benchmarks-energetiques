import socket
import sys
import struct
import multiprocessing

class GlobalState:
    def __init__(self, filename: str, connection, chunk_size: int, no_chunks: int):
        self.file = open(filename, 'wb') 
        self.connection = connection
        self.chunk_size = chunk_size
        self.no_chunks = no_chunks
        self.current_chunk = multiprocessing.Value('i', 0)  
        self.expected = multiprocessing.Value('i', 0)  
        self.count = multiprocessing.Value('i', 0) 
        self.lock = multiprocessing.Lock()  

def force_read(size: int, g_state: GlobalState) -> bytes:
    data = b''
    while len(data) != size:
        data += g_state.connection.recv(size - len(data))
    return data

def process_job(g_state):
    while True:
        g_state.lock.acquire()

        if g_state.count.value < g_state.no_chunks:
            received_chunk_id = int.from_bytes(g_state.connection.recv(8), 'little')
            data = force_read(g_state.chunk_size, g_state)

            g_state.lock.release()

            while received_chunk_id != g_state.expected.value:
                pass

            g_state.lock.acquire()
                
            g_state.file.write(data)
            g_state.file.flush()
            g_state.count.value += 1
            g_state.expected.value += 1
            
        g_state.lock.release()

        if g_state.count.value >= g_state.no_chunks:
            break

def main(port: int, outfile: str, proc_count: int):
    host = socket.gethostname()

    client = socket.socket()
    client.connect((host, port))

    info = client.recv(48)

    file_name = info[:32].decode()
    file_size = struct.unpack('<Q', info[32:40])[0]
    chunk_size = struct.unpack('<Q', info[40:48])[0]

    print(f"Receiving file: {file_name} ({file_size} bytes), with chunk size: {chunk_size}")

    chunk_size = int(chunk_size)
    file_size = int(file_size)
    no_chunks = file_size // chunk_size

    g_state = GlobalState(outfile, client, chunk_size, int(no_chunks))
    processes = []
    for _ in range(proc_count):
        proc = multiprocessing.Process(target=process_job, args=(g_state,))
        processes.append(proc)
        proc.start()

    for proc in processes:
        proc.join()

    client.send(b'ACK\x00')
    client.close()

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} [port] [filename] [proc_count]")
        print(f"Example: {sys.argv[0]} 4000 random_file 3")
        sys.exit(1)

    main(int(sys.argv[1]), sys.argv[2], int(sys.argv[3]))
