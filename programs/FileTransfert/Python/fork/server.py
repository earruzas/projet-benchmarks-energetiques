import socket
import os
import sys
import struct
import multiprocessing

class GlobalState:
    def __init__(self, file_name: str, connection, chunk_size: int, no_chunks: int):
        self.file_name = file_name
        self.connection = connection
        self.chunk_size = chunk_size
        self.no_chunks = int(no_chunks)
        self.current_chunk = multiprocessing.Value('i', 0)
        self.lock = multiprocessing.Lock()

# Function to be executed by each process
def process_job(g_state):
    file = open(g_state.file_name, 'rb')

    while True:
        g_state.lock.acquire()
            
        file.seek(g_state.current_chunk.value * g_state.chunk_size)
        data = file.read(g_state.chunk_size)

        # Send the correct chunk ID along with the data
        g_state.connection.send(struct.pack('<Q', g_state.current_chunk.value) + data)
        g_state.current_chunk.value += 1

        g_state.lock.release()

        if g_state.current_chunk.value >= g_state.no_chunks:
            # Exit the loop if all blocks have been processed
            break 

# Main Server
def main(port: int, file_name: str, chunk_size: int, proc_count: int):
    
    # Path of the file and size
    if not os.path.exists(file_name):
        print(f"File '{file_name}' not found.")
        sys.exit(1)
    
    file_size = os.path.getsize(file_name)
    print("Server on...")
    host = socket.gethostname()
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Reuse port
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((host, port))
    server_socket.listen(1)

    connection, _ = server_socket.accept()


    # Send basic file and chunks information
    data = file_name[:32].encode().ljust(32, b'\x00') + struct.pack('<Q', file_size) + struct.pack('<Q', chunk_size)
    connection.send(data)

    print(f"Sending file: {file_name} ({file_size} bytes) with chunk size: {chunk_size}")

    no_chunks = file_size // int(chunk_size)

    g_state = GlobalState(file_name, connection, int(chunk_size), no_chunks)
    processes = []
    for _ in range(proc_count):
        proc = multiprocessing.Process(target=process_job, args=(g_state,))
        processes.append(proc)
        proc.start()
    
    for proc in processes:
        proc.join()

    # Receive ack
    connection.recv(4)

    connection.close()
    server_socket.close()

# Dynamically enter arguments
if __name__ == '__main__':
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} [port] [file_name] [chunk_size] [proc_count]")
        print(f"Example: {sys.argv[0]} 4000 random_file 2048 3")
        sys.exit(1)

    main(int(sys.argv[1]), sys.argv[2], int(sys.argv[3]), int(sys.argv[4]))
