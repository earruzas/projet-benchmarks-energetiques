import socket
import os
import sys
import struct

# Initialize connection with a client and send basic details
def send_file(connection, chunk_size: int, file_path: str, file_size: int):
    chunk_id = 0
    no_block = file_size // chunk_size

    file = open(file_path, 'rb')

    for _ in range(no_block):
        data = file.read(chunk_size)
        connection.send(struct.pack('<Q', chunk_id) + data)
        chunk_id += 1
    
    # Receive ack
    data2 = connection.recv(4)
    ack = struct.unpack('4s', data2)[0]
    print(ack)
    file.close()

# Main server function
def main(port: int, file_name: str, chunk_size: int):

    # Path of the file and size
    file_path = f"./{file_name}"
    file_size = os.path.getsize(file_path)
    
    host = socket.gethostname()
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # todo reuse port
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((host, port))
    server_socket.listen(1)

    conn, _ = server_socket.accept()

    # Send basic file and chunks information
    file_name = file_name[:32].encode().ljust(32, b'\x00')
    data = file_name + struct.pack('<Q', file_size) + struct.pack('<Q', chunk_size)
    conn.send(data)

    print(f"Sending file: {file_name} ({file_size} bytes) with chunk size: {chunk_size}")

    send_file(conn, chunk_size, file_path, file_size)

    conn.close()
    server_socket.close()

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} [port] [file_name] [chunk_size]")
        print(f"Exemple: {sys.argv[0]} 4000 random_pic.jpg 2048")
        sys.exit(1)
    main(int(sys.argv[1]), sys.argv[2], int(sys.argv[3]))
