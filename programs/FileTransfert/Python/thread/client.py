import socket
import sys
import struct
import threading 

class GlobalState:
    def __init__(self, filename: str, connection, chunk_size: int, no_chunks: int):
        self.file = open(filename, 'wb')
        self.connection = connection
        self.chunk_size = chunk_size
        self.no_chunks = no_chunks
        self.current_chunk = 0
        self.expected = 0
        self.count = 0
        self.mutex = threading.Lock()

def force_read(size: int) -> bytes:
    data = b''
    while len(data) != size:
        data += g_state.connection.recv(size - len(data))
    return data

# Function to be executed by each thread
def thread_job():
    while True:
        g_state.mutex.acquire()

        if g_state.count < g_state.no_chunks:
            received_chunk_id = int.from_bytes(g_state.connection.recv(8), 'little')
            data = force_read(g_state.chunk_size)

            print(received_chunk_id, ' expected: ', g_state.expected)
            while received_chunk_id != g_state.expected:
                pass
                
            g_state.file.write(data)
            g_state.count += 1
            g_state.expected += 1
            
        g_state.mutex.release()

        if g_state.count >= g_state.no_chunks:
            break

# Main Client
def main(port: int, outfile: str, thread_count: int):
    host = socket.gethostname()  

    client = socket.socket()  
    client.connect((host, port)) 

    # Receive basic informations
    info = client.recv(48)
    print(f"Received data size: {len(info)} bytes")
    file_name = info[:32].decode()
    file_size = struct.unpack('<Q', info[32:40])[0]
    chunk_size = struct.unpack('<Q', info[40:48])[0]
    print(f"Receiving file: {file_name} ({file_size} bytes), with chunk size: {chunk_size}")
    
    chunk_size = int(chunk_size)
    file_size = int(file_size)
    no_chunks = file_size / chunk_size
    
    global g_state
    g_state = GlobalState(outfile, client, chunk_size, int(no_chunks))
    
    threads = []
    for _ in range(thread_count):
        thread = threading.Thread(target=thread_job)
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    client.send(b'ACK\x00')
    
    client.close()  

# Dynamically enter arguments
if __name__ == '__main__':
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} [port] [filename] [thread_count]")
        print(f"Exemple: {sys.argv[0]} 4000 random_file 3")
        sys.exit(1)

    main(int(sys.argv[1]), sys.argv[2], int(sys.argv[3]))
