import socket
import os
import sys
import struct
import threading

class GlobalState:
    def __init__(self, file_name: str, connection, chunk_size, no_chunks):
        self.file = open(file_name, 'rb')
        self.connection = connection
        self.chunk_size = chunk_size
        self.no_chunks = no_chunks
        self.current_chunk = 0
        self.mutex = threading.Lock()

# Function to be executed by each thread
def thread_job():
    while True:
        g_state.mutex.acquire()

        if g_state.current_chunk < g_state.no_chunks:
                data = g_state.file.read(g_state.chunk_size)
                g_state.connection.send(struct.pack('<Q', g_state.current_chunk) + data)
                g_state.current_chunk += 1
        
        g_state.mutex.release()

        if g_state.current_chunk >= g_state.no_chunks:
            # Exit the loop if all blocks have been processed
            break 

# Main server function
def main(port: int, file_name: str, chunk_size: int, thread_count: int):
    
    # Path of the file and size
    if not os.path.exists(file_name):
        print(f"File '{file_name}' not found.")
        sys.exit(1)
    
    file_size = os.path.getsize(file_name)
   
    host = socket.gethostname()
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Reuse port
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((host, port))
    server_socket.listen(1)

    connection, _ = server_socket.accept()

    # Send basic file and chunks information
    data = file_name[:32].encode().ljust(32, b'\x00') + struct.pack('<Q', file_size) + struct.pack('<Q', chunk_size)
    connection.send(data)
    print(f"Sending file: {file_name} ({file_size} bytes) with chunk size: {chunk_size}")

    no_chunks = file_size // int(chunk_size)
    global g_state
    g_state = GlobalState(file_name, connection, int(chunk_size), no_chunks)

    threads = []
    for i in range(thread_count):
        thread = threading.Thread(target=thread_job, name=f"Thread-{i}")
        threads.append(thread)
        thread.start()
    
    for _ in range(thread_count):
        thread.join()

    connection.recv(4)

    connection.close()
    server_socket.close()

# Dynamically enter arguments
if __name__ == '__main__':
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} [port] [file_name] [chunk_size] [thread_count]")
        print(f"Exemple: {sys.argv[0]} 4000 random_file 2048 3")
        sys.exit(1)

    main(int(sys.argv[1]), sys.argv[2], int(sys.argv[3]),int(sys.argv[4]))
