import os

from programs.FileTransfert.C.measure import do_measures_C
from programs.FileTransfert.Java.measure import do_measures_java
from programs.FileTransfert.Python.measure import do_measures_python

def do_all_measures(out_dir: str):
    os.makedirs(out_dir+'/FileTransfert', exist_ok=True)
    do_measures_C(out_dir)

    do_measures_java(out_dir+'/FileTransfert')

    do_measures_python(out_dir+'/FileTransfert')