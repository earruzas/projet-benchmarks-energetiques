from typing import Tuple
import re

perf_command = 'perf stat -B -e power/energy-cores/ -e power/energy-ram/ -e power/energy-psys/'


def parse_joules_seconds(data: bytes) -> Tuple[float, float, float, float, float]:
    cpu, ram, nic, sd, duration = 0, 0, 0, 0, 0
    for line in data.decode().split('\n'):
        group = re.findall(r'[0-9]+.[0-9]+', line)
        if len(group) == 0:
            continue

        if 'CPU' in line:
            cpu = float(group[0])
        elif 'RAM' in line:
            ram = float(group[0])
        elif 'NIC' in line:
            nic = float(group[0])
        elif 'SD' in line:
            sd = float(group[0])
        elif 'DURATION' in line:
            duration = float(group[0])

    return cpu, ram, nic, sd, duration