const green = '#0d7356';
const green2 = '#558F6E';
const green3 = '#69A27E';
const light_green = '#72b8a3';
const drak_green = '#0d3d2f';
const DefDatabase = 'Pour proposer un benchmark pertinent de base de données, il est nécessaire de sélectionner les bases de données les plus courantes. Le choix s\’est porté alors sur MariaDB et Neo4j. On a alors implémenté dans ces 2 bases de données des programmes qui vont réaliser des requêtes sur une base de données préalablement complète(base de données de tweets). Ces requêtes sont simples (c’est à dire une seule requête) ou bien multiples (plusieurs requêtes qui itèrent sur la précédente). On va ensuite comparer les résultats obtenus dans les 3 langages de programmation pré-sélectionnés.';
const DefBinaryTrees = 'Ce programme est implémenté à partir du modèle implémenté sur le site du CLBG. Le but de cet algorithme est de créer de parfait arbres binaires en utilisant un minimum d’  allocations possible tout en étant optimisé. On va ensuite comparer les résultats obtenus dans les 3 langages de programmation pré-sélectionnés. ';
const DefFileCompression = 'Un programme de compression d\'un ensemble de fichiers, en utilisant la méthode de compression GZIP. Cette méthode permet de faire varier le niveau de compression, ici 5.7.9 nous pourrons ainsi regarder l\'impact du niveau de compression sur le ratio de compression et la consommation électrique de celle-ci.';
const DefClassification = 'Ce programme va principalement implémenter la formation et l\'entraînement d\’un simple modèle de classification de texte réalisé en Python avec TensorFlow. Cet algorithme va classifier un jeu de données en deux catégories: positif ou négatif. Les résultats vont être divisés en plusieurs partie représentant chacuns des pourcentages du dataset utilisé : respectivement 25\%, 50\%, et 100\%';
const DefFileTransfer = 'Un programme de transfert de fichier. On va réaliser un transfert d\'un ou plusieurs fichiers de taille variant entre deux machines. Le programme va  se décliner en version synchronisée, une version utilisant des threads et une version asynchrone utilisant des processus lourds. On va ensuite comparer les résultats obtenus dans les 3 langages de programmation pré-sélectionnés.';
var singleData = {};
var singleData = {};
const labelsFixe = [
    { value: 175, label: 'voiture 800km' },
    { value: 207, label: 'avion 800km' },
    { value: 153, label: 'moto 800km' },
    { value: 88, label: 'Ordinateur fixe(par ans)' }
];
const SelectContainer = document.getElementById("tableContainer");
let tableHTML = '';
function CreateSelect(SelectName, Select1, Select2, Select3, Select4) {
    const SelectContainer = document.getElementById("SelectContainer");
    let newOptions = `
        <option value="${Select1}">${Select1}</option>
        <option value="${Select2}">${Select2}</option>
        `;
    if (Select3) {
        newOptions += `<option value="${Select3}">${Select3}</option>`;
    }
    if (Select4) {
        newOptions += `<option value="${Select4}">${Select4}</option>`;
    }
    const newSelect = `
        <select id="${SelectName}" class="form-select" aria-label="Default select example">
        ${newOptions}
        </select>
        `;
    SelectContainer.innerHTML += newSelect;
}
function CreateDefinition(name) {
    const DefContainer = document.getElementById("definition");
    let Defname = '';
    let Definition = ``;
    if (name === 'database') {
        Definition = `<h5 class="padding-32">${DefDatabase}</h5>`;
        Defname = 'Database Benchmark';
    }
    else if (name === 'binaryTrees') {
        Definition = `<h5 class="padding-32">${DefBinaryTrees}</h5>`;
        Defname = 'Binary Trees Benchmark';
    }
    else if (name === 'fileCompression') {
        Definition = `<h5 class="padding-32">${DefFileCompression}</h5>`;
        Defname = 'File Compression Benchmark';
    }
    else if (name === 'classification') {
        Definition = `<h5 class="padding-32">${DefClassification}</h5>`;
        Defname = 'Classification Benchmark';
    }
    else if (name === 'fileTransfer') {
        Definition = `<h5 class="padding-32">${DefFileTransfer}</h5>`;
        Defname = 'File Transfer Benchmark';
    }
    let newDef = `
    <h1>${Defname}</h1>
    ${Definition}
        `;
    DefContainer.innerHTML += newDef;
}
function RadarChart(data, id) {
    Chart.getChart(document.getElementById(id))?.destroy();
    const config = {
        type: 'radar',
        data: data,
        options: {
            elements: {
                line: {
                    borderWidth: 3
                }
            }
        },
    };
    const radarChart = new Chart(document.getElementById(id), config);
}
function horizontalBarChart(data, id) {
    Chart.getChart(document.getElementById(id))?.destroy();
    const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };
    const barChart2 = new Chart(document.getElementById(id), config);
}
function BarChart(data, id, label) {
    const chart_1 = document.getElementById('chart_1');
    if (!chart_1.querySelector('h1')) {
        var label = `<h1 class="large title"> ${label}</h1>
            `;
        chart_1.innerHTML += label;
    }
    Chart.getChart(document.getElementById(id))?.destroy();
    const config = {
        type: 'bar',
        data,
        options: {
            indexAxis: 'y',
        }
    };
    const barChart = new Chart(document.getElementById(id), config);
}
function createTable(dataArrays, keys, keys2) {
    const labels = {
        'cpu': 'CPU Usage',
        'ram': 'RAM Usage',
        'sd': 'Disk Usage',
        'nic': 'Network Usage',
        'duration': 'Duration',
        'compression': 'Compression Ratio',
    };
    var tableHTML = `<table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Source File</th>`;
    keys.forEach(key => {
        if (labels[key]) {
            tableHTML += `<th scope="col">${labels[key]}</th>`;
        }
    });
    for (let i = 0; i < dataArrays.length; i++) {
        tableHTML += `<tr>
                        <th scope="row">${keys2[i]}</th>`;
        keys.forEach(key => {
            const dataArray = dataArrays[i];
            const value = dataArray.find(obj => obj[key]);
            if (value) {
                tableHTML += `<td>${value[key]}</td>`;
            }
            else {
                tableHTML += `<td></td>`;
            }
        });
        tableHTML += `</tr>`;
    }
    return tableHTML;
}
function updateTableContent(data, keys, keys2) {
    SelectContainer.innerHTML = createTable(data, keys, keys2);
    ;
}
async function createVisualisationDatabase(data) {
    CreateSelect('benchmarkSelect', 'single', 'multiple');
    const singleData = filterBy(data, 'mode', 'single');
    const multipleData = filterBy(data, 'mode', 'multiple');
    const mariaDbSingle = filterBy(singleData, 'db', 'MariaDB');
    const neo4jSingle = filterBy(singleData, 'db', 'Neo4J');
    const mariaDbMultiple = filterBy(multipleData, 'db', 'MariaDB');
    const neo4jMultiple = filterBy(multipleData, 'db', 'Neo4J');
    const keys = ['cpu', 'ram', 'nic', 'sd', 'duration'];
    const keys2 = ['MariaDB Single Request', 'Neo4J single request', 'MariaDB multiple requests ', 'Neo4J nultiple request'];
    const keys3 = ['cpu', 'ram', 'nic', 'sd'];
    const mariadbSingleAvg = averageObject(mariaDbSingle, keys);
    const neo4jSingleAvg = averageObject(neo4jSingle, keys);
    const mariadbMultipleAvg = averageObject(mariaDbMultiple, keys);
    const neo4jMultipleAvg = averageObject(neo4jMultiple, keys);
    const BarData = [{ value: sumAverageObject(mariadbSingleAvg, keys3), label: "MariaDB Single Request" }, { value: sumAverageObject(neo4jSingleAvg, keys3), label: "Neo4J single request" }, { value: sumAverageObject(mariadbMultipleAvg, keys3), label: "MariaDB multiple requests" }, { value: sumAverageObject(neo4jMultipleAvg, keys3), label: "Neo4J multiple request" }];
    let normalizedData_single = normalizeDatasetObjects([createDatasetObject(filterBy(mariaDbSingle, 'language', 'C'), keys), createDatasetObject(filterBy(mariaDbSingle, 'language', 'Java'), keys), createDatasetObject(filterBy(mariaDbSingle, 'language', 'Python'), keys)], keys);
    let normalizedData_multiple = normalizeDatasetObjects([createDatasetObject(filterBy(mariaDbMultiple, 'language', 'C'), keys), createDatasetObject(filterBy(mariaDbMultiple, 'language', 'Java'), keys), createDatasetObject(filterBy(mariaDbMultiple, 'language', 'Python'), keys)], keys);
    const radarChartContainer = document.getElementById('radarChart');
    let radarChartInstance = Chart.getChart(radarChartContainer);
    if (radarChartInstance) {
        radarChartInstance.destroy();
    }
    RadarChart(createDataForRadarChart(normalizedData_single, keys, ['C', 'Java', 'Python']), 'radarChart');
    BarChart(createDataForBarChart(conversion(BarData, 1000000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission d\'un milliards d\'exécutions du programme (en kgC02)');
    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
    SelectContainer.innerHTML = createTable([mariadbSingleAvg, neo4jSingleAvg, mariadbMultipleAvg, neo4jMultipleAvg], keys, keys2);
    document.getElementById('benchmarkSelect').addEventListener('change', function () {
        const selectedValue = this.value;
        if (selectedValue === 'single') {
            radarChartInstance = Chart.getChart(radarChartContainer);
            if (radarChartInstance) {
                radarChartInstance.destroy();
            }
            RadarChart(createDataForRadarChart(normalizedData_single, keys, ['C', 'Java', 'Python']), 'radarChart');
        }
        else if (selectedValue === 'multiple') {
            radarChartInstance = Chart.getChart(radarChartContainer);
            if (radarChartInstance) {
                radarChartInstance.destroy();
            }
            RadarChart(createDataForRadarChart(normalizedData_multiple, keys, ['C', 'Java', 'Python']), 'radarChart');
        }
    });
}
async function createVizualisationBinaryTrees(data) {
    const data_C = filterBy(data, 'language', 'C');
    const data_java = filterBy(data, 'language', 'Java');
    const data_python = filterBy(data, 'language', 'Python');
    const keys = ['duration', 'ram', 'cpu', 'sd', 'nic'];
    const keys3 = ['ram', 'cpu', 'nic'];
    const dataset_C = createDatasetObject(data_C, keys);
    const dataset_Java = createDatasetObject(data_java, keys);
    const dataset_Python = createDatasetObject(data_python, keys);
    const BarData = [{ value: sumAverageObject(averageObject(data_C, keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(data_java, keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(data_python, keys), keys3), label: "Python" }];
    let normalizedData = normalizeDatasetObjects([dataset_C, dataset_Java, dataset_Python], keys);
    const radarChartContainer = document.getElementById('radarChart');
    let radarChartInstance = Chart.getChart(radarChartContainer);
    if (radarChartInstance) {
        radarChartInstance.destroy();
    }
    RadarChart(createDataForRadarChart(normalizedData, keys, ['C', 'Java', 'Python']), 'radarChart');
    BarChart(createDataForBarChart(conversion(BarData, 10000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 10 000 000 d\'exécutions du programme (en kgC02)');
    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
    SelectContainer.innerHTML = createTable([averageObject(data_C, keys), averageObject(data_java, keys), averageObject(data_python, keys)], keys, ['C', 'Java', 'Python']);
}
async function createVisualisationFileCompression(data) {
    CreateSelect('compressionSelectLevel', 'Compression-level: 5', 'Compression-level: 7', 'Compression-level: 9');
    CreateSelect('compressionSelectBuffer', 'Buffer: 65536', 'Buffer: 262144', 'Buffer: 1048576');
    const data_C = filterBy(data, 'language', 'C');
    const data_java = filterBy(data, 'language', 'Java');
    const data_python = filterBy(data, 'language', 'Python');
    const data_C_5 = filterBy(data_C, 'compression-level', '5');
    const data_C_7 = filterBy(data_C, 'compression-level', '7');
    const data_C_9 = filterBy(data_C, 'compression-level', '9');
    const data_java_5 = filterBy(data_java, 'compression-level', '5');
    const data_java_7 = filterBy(data_java, 'compression-level', '7');
    const data_java_9 = filterBy(data_java, 'compression-level', '9');
    const data_python_5 = filterBy(data_python, 'compression-level', '5');
    const data_python_7 = filterBy(data_python, 'compression-level', '7');
    const data_python_9 = filterBy(data_python, 'compression-level', '9');
    const keys = ['cpu', 'ram', 'sd', 'compression', 'duration'];
    const keys2 = ['C', 'Java', 'Python'];
    const keys3 = ['cpu', 'ram', 'sd'];
    let BarData = [{ value: sumAverageObject(averageObject(data_C_5, keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(data_java_5, keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(data_python_5, keys), keys3), label: "Python" }];
    let normalizedData5 = normalizeDatasetObjects([createDatasetObject(data_C_5, keys), createDatasetObject(data_java_5, keys), createDatasetObject(data_python_5, keys)], keys);
    RadarChart(createDataForRadarChart(normalizedData5, keys, keys2), 'radarChart');
    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 10 000 000 d\'exécutions du programme (en kgC02)');
    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C, 'compression-level', '5'), keys), averageObject(filterBy(data_java, 'compression-level', '5'), keys), averageObject(filterBy(data_python, 'compression-level', '5'), keys)], keys, keys2);
    function updateCharts(level, buffer) {
        if (level === 'Compression-level: 5') {
            if (buffer === 'Buffer: 65536') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_5, 'buffer-size', '65536'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_5, 'buffer-size', '65536'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_5, 'buffer-size', '65536'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_5, 'buffer-size', '65536'), keys), createDatasetObject(filterBy(data_java_5, 'buffer-size', '65536'), keys), createDatasetObject(filterBy(data_python_5, 'buffer-size', '65536'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (buffer === 'Buffer: 262144') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_5, 'buffer-size', '262144'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_5, 'buffer-size', '262144'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_5, 'buffer-size', '262144'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_5, 'buffer-size', '262144'), keys), createDatasetObject(filterBy(data_java_5, 'buffer-size', '262144'), keys), createDatasetObject(filterBy(data_python_5, 'buffer-size', '262144'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (buffer === 'Buffer: 1048576') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_5, 'buffer-size', '1048576'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_5, 'buffer-size', '1048576'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_5, 'buffer-size', '1048576'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_5, 'buffer-size', '1048576'), keys), createDatasetObject(filterBy(data_java_5, 'buffer-size', '1048576'), keys), createDatasetObject(filterBy(data_python_5, 'buffer-size', '1048576'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
        }
        else if (level === 'Compression-level: 7') {
            if (buffer === 'Buffer: 65536') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_7, 'buffer-size', '65536'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_7, 'buffer-size', '65536'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_7, 'buffer-size', '65536'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_7, 'buffer-size', '65536'), keys), createDatasetObject(filterBy(data_java_7, 'buffer-size', '65536'), keys), createDatasetObject(filterBy(data_python_7, 'buffer-size', '65536'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (buffer === 'Buffer: 262144') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_7, 'buffer-size', '262144'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_7, 'buffer-size', '262144'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_7, 'buffer-size', '262144'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_7, 'buffer-size', '262144'), keys), createDatasetObject(filterBy(data_java_7, 'buffer-size', '262144'), keys), createDatasetObject(filterBy(data_python_7, 'buffer-size', '262144'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (buffer === 'Buffer: 1048576') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_7, 'buffer-size', '1048576'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_7, 'buffer-size', '1048576'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_7, 'buffer-size', '1048576'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_7, 'buffer-size', '1048576'), keys), createDatasetObject(filterBy(data_java_7, 'buffer-size', '1048576'), keys), createDatasetObject(filterBy(data_python_7, 'buffer-size', '1048576'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
        }
        else if (level === 'Compression-level: 9') {
            if (buffer === 'Buffer: 65536') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_9, 'buffer-size', '65536'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_9, 'buffer-size', '65536'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_9, 'buffer-size', '65536'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_9, 'buffer-size', '65536'), keys), createDatasetObject(filterBy(data_java_9, 'buffer-size', '65536'), keys), createDatasetObject(filterBy(data_python_9, 'buffer-size', '65536'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (buffer === 'Buffer: 262144') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_9, 'buffer-size', '262144'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_9, 'buffer-size', '262144'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_9, 'buffer-size', '262144'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_9, 'buffer-size', '262144'), keys), createDatasetObject(filterBy(data_java_9, 'buffer-size', '262144'), keys), createDatasetObject(filterBy(data_python_9, 'buffer-size', '262144'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (buffer === 'Buffer: 1048576') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_9, 'buffer-size', '1048576'), keys), keys3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_9, 'buffer-size', '1048576'), keys), keys3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_9, 'buffer-size', '1048576'), keys), keys3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_9, 'buffer-size', '1048576'), keys), createDatasetObject(filterBy(data_java_9, 'buffer-size', '1048576'), keys), createDatasetObject(filterBy(data_python_9, 'buffer-size', '1048576'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
        }
    }
    function updateTable(level, buffer) {
        if (level === 'Compression-level: 5') {
            if (buffer === 'Buffer: 65536') {
                SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_5, 'buffer-size', '65536'), keys), averageObject(filterBy(data_java, 'buffer-size', '65536'), keys), averageObject(filterBy(data_python, 'buffer-size', '65536'), keys)], keys, keys2);
            }
            else if (buffer === 'Buffer: 262144') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '262144'), keys), averageObject(filterBy(data_java, 'buffer-size', '262144'), keys), averageObject(filterBy(data_python, 'buffer-size', '262144'), keys)], keys, keys2);
            }
            else if (buffer === 'Buffer: 1048576') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '1048576'), keys), averageObject(filterBy(data_java, 'buffer-size', '1048576'), keys), averageObject(filterBy(data_python, 'buffer-size', '1048576'), keys)], keys, keys2);
            }
        }
        else if (level === 'Compression-level: 7') {
            if (buffer === 'Buffer: 65536') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '65536'), keys), averageObject(filterBy(data_java, 'buffer-size', '65536'), keys), averageObject(filterBy(data_python, 'buffer-size', '65536'), keys)], keys, keys2);
            }
            else if (buffer === 'Buffer: 262144') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '262144'), keys), averageObject(filterBy(data_java, 'buffer-size', '262144'), keys), averageObject(filterBy(data_python, 'buffer-size', '262144'), keys)], keys, keys2);
            }
            else if (buffer === 'Buffer: 1048576') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '1048576'), keys), averageObject(filterBy(data_java, 'buffer-size', '1048576'), keys), averageObject(filterBy(data_python, 'buffer-size', '1048576'), keys)], keys, keys2);
            }
        }
        else if (level === 'Compression-level: 9') {
            if (buffer === 'Buffer: 65536') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '65536'), keys), averageObject(filterBy(data_java, 'buffer-size', '65536'), keys), averageObject(filterBy(data_python, 'buffer-size', '65536'), keys)], keys, keys2);
            }
            else if (buffer === 'Buffer: 262144') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '262144'), keys), averageObject(filterBy(data_java, 'buffer-size', '262144'), keys), averageObject(filterBy(data_python, 'buffer-size', '262144'), keys)], keys, keys2);
            }
            else if (buffer === 'Buffer: 1048576') {
                updateTableContent([averageObject(filterBy(data_C, 'buffer-size', '1048576'), keys), averageObject(filterBy(data_java, 'buffer-size', '1048576'), keys), averageObject(filterBy(data_python, 'buffer-size', '1048576'), keys)], keys, keys2);
            }
        }
    }
    document.getElementById('compressionSelectLevel').addEventListener('change', function () {
        const selectedValue = this.value;
        const buffer = document.getElementById('compressionSelectBuffer').value;
        updateTable(selectedValue, buffer);
        updateCharts(selectedValue, buffer);
    });
    document.getElementById('compressionSelectBuffer').addEventListener('change', function () {
        const selectedValue = this.value;
        const level = document.getElementById('compressionSelectLevel').value;
        updateTable(level, selectedValue);
        updateCharts(level, selectedValue);
    });
}
async function createVisualisationClassification(data) {
    CreateSelect('ClassificationSelectMode', 'entraînement', 'Prédiction');
    const data_train = filterBy(data, 'mode', 'train');
    const data_predict = filterBy(data, 'mode', 'predict');
    const data_predict_25 = filterBy(data_predict, 'dataset-size', '25%');
    const data_predict_50 = filterBy(data_predict, 'dataset-size', '50%');
    const data_predict_100 = filterBy(data_predict, 'dataset-size', '100%');
    const data_train_25 = filterBy(data_train, 'dataset-size', '25%');
    const data_train_50 = filterBy(data_train, 'dataset-size', '50%');
    const data_train_100 = filterBy(data_train, 'dataset-size', '100%');
    const keys = ['cpu', 'ram', 'nic', 'sd', 'duration'];
    const keys3 = ['cpu', 'ram', 'sd'];
    const dataset_train_25 = createDatasetObject(data_train_25, keys);
    let BarData = [{ value: sumAverageObject(averageObject(data_train_25, keys), keys3), label: "25%" }, { value: sumAverageObject(averageObject(data_train_50, keys), keys3), label: "50%" }, { value: sumAverageObject(averageObject(data_train_100, keys), keys3), label: "100%" }];
    let normalizedData = normalizeDatasetObjects([createDatasetObject(data_train_25, keys), createDatasetObject(data_train_50, keys), createDatasetObject(data_train_100, keys)], keys);
    RadarChart(createDataForRadarChart(normalizedData, keys, ['Entraînement']), 'radarChart');
    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
    SelectContainer.innerHTML = createTable([averageObject(data_train_25, keys), averageObject(data_train_50, keys), averageObject(data_train_100, keys)], keys, ['25%', '50%', '100%']);
    function updateCharts(mode) {
        if (mode === 'entraînement') {
            let BarData = [{ value: sumAverageObject(averageObject(data_train_25, keys), keys3), label: "25%" }, { value: sumAverageObject(averageObject(data_train_50, keys), keys3), label: "50%" }, { value: sumAverageObject(averageObject(data_train_100, keys), keys3), label: "100%" }];
            RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(data_train_25, keys), createDatasetObject(data_train_50, keys), createDatasetObject(data_train_100, keys)], keys), keys, ['25%', '50%', '100%']), 'radarChart');
            BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
            horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
        }
        else if (mode === 'Prédiction') {
            let BarData = [{ value: sumAverageObject(averageObject(data_predict_25, keys), keys3), label: "25%" }, { value: sumAverageObject(averageObject(data_predict_50, keys), keys3), label: "50%" }, { value: sumAverageObject(averageObject(data_predict_100, keys), keys3), label: "100%" }];
            RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(data_predict_25, keys), createDatasetObject(data_predict_50, keys), createDatasetObject(data_predict_100, keys)], keys), keys, ['25%', '50%', '100%']), 'radarChart');
            BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
            horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
        }
    }
    function updateTable(mode) {
        if (mode === 'entraînement') {
            SelectContainer.innerHTML = createTable([averageObject(data_train_25, keys), averageObject(data_train_50, keys), averageObject(data_train_100, keys)], keys, ['25%', '50%', '100%']);
        }
        else if (mode === 'Prédiction') {
            SelectContainer.innerHTML = createTable([averageObject(data_predict_25, keys), averageObject(data_predict_50, keys), averageObject(data_predict_100, keys)], keys, ['25%', '50%', '100%']);
        }
    }
    document.getElementById('ClassificationSelectMode').addEventListener('change', function () {
        const selectedValue = this.value;
        updateTable(selectedValue);
        updateCharts(selectedValue);
    });
}
async function createVisualisationFileTransfer(data) {
    CreateSelect('FiletransferSelectMode', 'Synchrone', 'Thread', 'Fork');
    CreateSelect('FiletransferSelectBloc', 'taille: 4096', 'taille: 65536', 'taille: 1048576');
    CreateSelect('FiletransferSelectThread', '3 threads', '5 threads', '10 threads');
    const data_C = filterBy(data, 'language', 'C');
    const data_java = filterBy(data, 'language', 'Java');
    const data_python = filterBy(data, 'language', 'Python');
    const keys = ['cpu', 'ram', 'nic', 'sd', 'duration'];
    const keys2 = ['C', 'Java', 'Python'];
    const key3 = ['cpu', 'ram', 'nic', 'sd'];
    const data_C_monothread = filterBy(data_C, 'mode', 'monothread');
    const data_java_monothread = filterBy(data_java, 'mode', 'monothread');
    const data_python_monothread = filterBy(data_python, 'mode', 'monothread');
    const data_C_fork = filterBy(data_C, 'mode', 'fork');
    const data_C_fork_4096 = filterBy(data_C_fork, 'block-size', '4096');
    const data_C_fork_65536 = filterBy(data_C_fork, 'block-size', '65536');
    const data_C_fork_1048576 = filterBy(data_C_fork, 'block-size', '1048576');
    const data_C_thread = filterBy(data_C, 'mode', 'thread');
    const data_java_thread = filterBy(data_java, 'mode', 'thread');
    const data_python_thread = filterBy(data_python, 'mode', 'thread');
    const data_C_thread_4096 = filterBy(data_C_thread, 'block-size', '4096');
    const data_C_thread_65536 = filterBy(data_C_thread, 'block-size', '65536');
    const data_C_thread_1048576 = filterBy(data_C_thread, 'block-size', '1048576');
    const data_python_thread_4096 = filterBy(data_python_thread, 'block-size', '4096');
    const data_python_thread_65536 = filterBy(data_python_thread, 'block-size', '65536');
    const data_python_thread_1048576 = filterBy(data_python_thread, 'block-size', '1048576');
    const data_java_thread_4096 = filterBy(data_java_thread, 'block-size', '4096');
    const data_java_thread_65536 = filterBy(data_java_thread, 'block-size', '65536');
    const data_java_thread_1048576 = filterBy(data_java_thread, 'block-size', '1048576');
    const BarData = [{ value: sumAverageObject(averageObject(data_C, keys), key3), label: "C" }, { value: sumAverageObject(averageObject(data_java, keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(data_python, keys), key3), label: "Python" }];
    const normalizeData_monothread = normalizeDatasetObjects([createDatasetObject(filterBy(data_C, 'mode', 'monothread'), keys), createDatasetObject(filterBy(data_java, 'mode', 'monothread'), keys), createDatasetObject(filterBy(data_python, 'mode', 'monothread'), keys)], keys);
    RadarChart(createDataForRadarChart(normalizeData_monothread, keys, keys2), 'radarChart');
    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', ' Tableaux de comparaison avec l\'emission de x10 000 000 d\'executions du programme(en kgC02)');
    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C, 'mode', 'monothread'), keys), averageObject(filterBy(data_java, 'mode', 'monothread'), keys), averageObject(filterBy(data_python, 'mode', 'monothread'), keys)], keys, keys2);
    function updateCharts(mode, blockSize, threadCount) {
        if (mode === 'Synchrone') {
            if (blockSize === 'taille: 4096') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_monothread, 'block-size', '4096'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_monothread, 'block-size', '4096'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_monothread, 'block-size', '4096'), keys), key3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_monothread, 'block-size', '4096'), keys), createDatasetObject(filterBy(data_java_monothread, 'block-size', '4096'), keys), createDatasetObject(filterBy(data_python_monothread, 'block-size', '4096'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (blockSize === 'taille: 65536') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_monothread, 'block-size', '65536'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_monothread, 'block-size', '65536'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_monothread, 'block-size', '65536'), keys), key3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_monothread, 'block-size', '65536'), keys), createDatasetObject(filterBy(data_java_monothread, 'block-size', '65536'), keys), createDatasetObject(filterBy(data_python_monothread, 'block-size', '65536'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
            else if (blockSize === 'taille: 1048576') {
                let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_monothread, 'block-size', '1048576'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_monothread, 'block-size', '1048576'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_monothread, 'block-size', '1048576'), keys), key3), label: "Python" }];
                RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_monothread, 'block-size', '1048576'), keys), createDatasetObject(filterBy(data_java_monothread, 'block-size', '1048576'), keys), createDatasetObject(filterBy(data_python_monothread, 'block-size', '1048576'), keys)], keys), keys, keys2), 'radarChart');
                BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
            }
        }
        else if (mode === 'Thread') {
            if (threadCount === '3 threads') {
                if (blockSize === 'taille: 4096') {
                    console.log(filterBy(data_C_thread_4096, 'thread', '3'));
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_4096, 'thread', '3'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_4096, 'thread', '3'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_4096, 'thread', '3'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_4096, 'thread', '3'), keys), createDatasetObject(filterBy(data_java_thread_4096, 'thread', '3'), keys), createDatasetObject(filterBy(data_python_thread_4096, 'thread', '3'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                    console.log(BarData);
                }
                else if (blockSize === 'taille: 65536') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_65536, 'thread', '3'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_65536, 'thread', '3'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_65536, 'thread', '3'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_65536, 'thread', '3'), keys), createDatasetObject(filterBy(data_java_thread_65536, 'thread', '3'), keys), createDatasetObject(filterBy(data_python_thread_65536, 'thread', '3'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 1048576') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_1048576, 'thread', '3'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_1048576, 'thread', '3'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_1048576, 'thread', '3'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_1048576, 'thread', '3'), keys), createDatasetObject(filterBy(data_java_thread_1048576, 'thread', '3'), keys), createDatasetObject(filterBy(data_python_thread_1048576, 'thread', '3'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
            }
            else if (threadCount === '5 threads') {
                if (blockSize === 'taille: 4096') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_4096, 'thread', '5'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_4096, 'thread', '5'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_4096, 'thread', '5'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_4096, 'thread', 5), keys), createDatasetObject(filterBy(data_java_thread_4096, 'thread', '5'), keys), createDatasetObject(filterBy(data_python_thread_4096, 'thread', '5'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 65536') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_65536, 'thread', '5'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_65536, 'thread', '5'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_65536, 'thread', '5'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_65536, 'thread', '5'), keys), createDatasetObject(filterBy(data_java_thread_65536, 'thread', '5'), keys), createDatasetObject(filterBy(data_python_thread_65536, 'thread', '5'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 1048576') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_1048576, 'thread', '5'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_1048576, 'thread', '5'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_1048576, 'thread', '5'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_1048576, 'thread', '5'), keys), createDatasetObject(filterBy(data_java_thread_1048576, 'thread', '5'), keys), createDatasetObject(filterBy(data_python_thread_1048576, 'thread', '5'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
            }
            else if (threadCount === '10 threads') {
                if (blockSize === 'taille: 4096') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_4096, 'thread', '10'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_4096, 'thread', '10'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_4096, 'thread', '10'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_4096, 'thread', '10'), keys), createDatasetObject(filterBy(data_java_thread_4096, 'thread', '10'), keys), createDatasetObject(filterBy(data_python_thread_4096, 'thread', '10'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 65536') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_65536, 'thread', '10'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_65536, 'thread', '10'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_65536, 'thread', '10'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_65536, 'thread', '10'), keys), createDatasetObject(filterBy(data_java_thread_65536, 'thread', '10'), keys), createDatasetObject(filterBy(data_python_thread_65536, 'thread', '10'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 1048576') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_thread_1048576, 'thread', '10'), keys), key3), label: "C" }, { value: sumAverageObject(averageObject(filterBy(data_java_thread_1048576, 'thread', '10'), keys), key3), label: "Java" }, { value: sumAverageObject(averageObject(filterBy(data_python_thread_1048576, 'thread', '10'), keys), key3), label: "Python" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_thread_1048576, 'thread', '10'), keys), createDatasetObject(filterBy(data_java_thread_1048576, 'thread', '10'), keys), createDatasetObject(filterBy(data_python_thread_1048576, 'thread', '10'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
            }
        }
        else if (mode === 'Fork') {
            if (threadCount === '3 threads') {
                if (blockSize === 'taille: 4096') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_4096, 'thread', '3'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_4096, 'thread', '3'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 65536') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_65536, 'thread', '3'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_65536, 'thread', '3'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 1048576') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_1048576, 'thread', '3'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_1048576, 'thread', '3'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
            }
            else if (threadCount === '5 threads') {
                if (blockSize === 'taille: 4096') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_4096, 'thread', '5'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_4096, 'thread', '5'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 65536') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_65536, 'thread', '5'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_65536, 'thread', '5'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 1048576') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_1048576, 'thread', '5'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_1048576, 'thread', '5'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
            }
            else if (threadCount === '10 threads') {
                if (blockSize === 'taille: 4096') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_4096, 'thread', '10'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_4096, 'thread', '10'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 65536') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_65536, 'thread', '10'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_65536, 'thread', '10'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
                else if (blockSize === 'taille: 1048576') {
                    let BarData = [{ value: sumAverageObject(averageObject(filterBy(data_C_fork_1048576, 'thread', '10'), keys), key3), label: "C" }];
                    RadarChart(createDataForRadarChart(normalizeDatasetObjects([createDatasetObject(filterBy(data_C_fork_1048576, 'thread', '10'), keys)], keys), keys, keys2), 'radarChart');
                    BarChart(createDataForBarChart(conversion(BarData, 100000000).concat(labelsFixe)), 'barChart', 'Comparaison avec l\'émission de 100 000 000 d\'exécutions du programme (en kgC02)');
                    horizontalBarChart(createDataForBarChart(BarData), 'barChart2');
                }
            }
        }
    }
    function updateTable(mode, blockSize, threadCount) {
        if (mode === 'Synchrone') {
            if (blockSize === 'taille: 4096') {
                SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_monothread, 'block-size', '4096'), keys), averageObject(filterBy(data_java_monothread, 'block-size', '4096'), keys), averageObject(filterBy(data_python_monothread, 'block-size', '4096'), keys)], keys, keys2);
            }
            else if (blockSize === 'taille: 65536') {
                SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_monothread, 'block-size', '65536'), keys), averageObject(filterBy(data_java_monothread, 'block-size', '65536'), keys), averageObject(filterBy(data_python_monothread, 'block-size', '65536'), keys)], keys, keys2);
            }
            else if (blockSize === 'taille: 1048576') {
                SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_monothread, 'block-size', '1048576'), keys), averageObject(filterBy(data_java_monothread, 'block-size', '1048576'), keys), averageObject(filterBy(data_python_monothread, 'block-size', '1048576'), keys)], keys, keys2);
            }
        }
        else if (mode === 'Thread') {
            if (threadCount === '3 threads') {
                if (blockSize === 'taille: 4096') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_4096, 'thread', '3'), keys), averageObject(filterBy(data_java_thread_4096, 'thread', '3'), keys), averageObject(filterBy(data_python_thread_4096, 'thread', '3'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 65536') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_65536, 'thread', '3'), keys), averageObject(filterBy(data_java_thread_65536, 'thread', '3'), keys), averageObject(filterBy(data_python_thread_65536, 'thread', '3'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 1048576') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_1048576, 'thread', '3'), keys), averageObject(filterBy(data_java_thread_1048576, 'thread', '3'), keys), averageObject(filterBy(data_python_thread_1048576, 'thread', '3'), keys)], keys, keys2);
                }
            }
            else if (threadCount === '5 threads') {
                if (blockSize === 'taille: 4096') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_4096, 'thread', '5'), keys), averageObject(filterBy(data_java_thread_4096, 'thread', '5'), keys), averageObject(filterBy(data_python_thread_4096, 'thread', '5'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 65536') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_65536, 'thread', '5'), keys), averageObject(filterBy(data_java_thread_65536, 'thread', '5'), keys), averageObject(filterBy(data_python_thread_65536, 'thread', '5'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 1048576') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_1048576, 'thread', '5'), keys), averageObject(filterBy(data_java_thread_1048576, 'thread', '5'), keys), averageObject(filterBy(data_python_thread_1048576, 'thread', '5'), keys)], keys, keys2);
                }
            }
            else if (threadCount === '10 threads') {
                if (blockSize === 'taille: 4096') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_4096, 'thread', '10'), keys), averageObject(filterBy(data_java_thread_4096, 'thread', '10'), keys), averageObject(filterBy(data_python_thread_4096, 'thread', '10'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 65536') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_65536, 'thread', '10'), keys), averageObject(filterBy(data_java_thread_65536, 'thread', '10'), keys), averageObject(filterBy(data_python_thread_65536, 'thread', '10'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 1048576') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_1048576, 'thread', '10'), keys), averageObject(filterBy(data_java_thread_1048576, 'thread', '10'), keys), averageObject(filterBy(data_python_thread_1048576, 'thread', '10'), keys)], keys, keys2);
                }
            }
        }
        else if (mode === 'Fork') {
            if (threadCount === '3 threads') {
                if (blockSize === 'taille: 4096') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_4096, 'thread', '3'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 65536') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_thread_65536, 'thread', '3'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 1048576') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_1048576, 'thread', '3'), keys)], keys, keys2);
                }
            }
            else if (threadCount === '5 threads') {
                if (blockSize === 'taille: 4096') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_4096, 'thread', '5'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 65536') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_65536, 'thread', '5'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 1048576') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_1048576, 'thread', '5'), keys)], keys, keys2);
                }
            }
            else if (threadCount === '10 threads') {
                if (blockSize === 'taille: 4096') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_4096, 'thread', '10'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 65536') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_65536, 'thread', '10'), keys)], keys, keys2);
                }
                else if (blockSize === 'taille: 1048576') {
                    SelectContainer.innerHTML = createTable([averageObject(filterBy(data_C_fork_1048576, 'thread', '10'), keys)], keys, keys2);
                }
            }
        }
    }
    document.getElementById('FiletransferSelectMode').addEventListener('change', function () {
        const selectedValue = this.value;
        const blockSize = document.getElementById('FiletransferSelectBloc').value;
        const threadCount = document.getElementById('FiletransferSelectThread').value;
        updateTable(selectedValue, blockSize, threadCount);
        updateCharts(selectedValue, blockSize, threadCount);
    });
    document.getElementById('FiletransferSelectBloc').addEventListener('change', function () {
        const selectedValue = this.value;
        const mode = document.getElementById('FiletransferSelectMode').value;
        const threadCount = document.getElementById('FiletransferSelectThread').value;
        updateTable(mode, selectedValue, threadCount);
        updateCharts(mode, selectedValue, threadCount);
    });
    document.getElementById('FiletransferSelectThread').addEventListener('change', function () {
        const selectedValue = this.value;
        const blockSize = document.getElementById('FiletransferSelectBloc').value;
        const mode = document.getElementById('FiletransferSelectMode').value;
        updateTable(mode, blockSize, selectedValue);
        updateCharts(mode, blockSize, selectedValue);
    });
}
async function createViz(filePath, type) {
    try {
        const data = await parseFile(filePath);
        if (type === 'database') {
            createVisualisationDatabase(data);
        }
        else if (type === 'binaryTrees') {
            createVizualisationBinaryTrees(data);
        }
        else if (type === 'fileCompression') {
            createVisualisationFileCompression(data);
        }
        else if (type === 'classification') {
            createVisualisationClassification(data);
        }
        else if (type === 'fileTransfer') {
            createVisualisationFileTransfer(data);
        }
    }
    catch (error) {
        console.error('Error loading data:', error);
    }
}
function onLoadBenchmark() {
    const urlParams = new URLSearchParams(window.location.search);
    const benchmarkType = urlParams.get('type');
    let benchmarkName = 'Benchmark';
    let benchmarkDesc = '';
    if (benchmarkType === 'database') {
        benchmarkName = 'Database Benchmark';
        benchmarkDesc = 'Benchmark de requêtes sur une base de donnée ';
        createViz('ressources/measures/measures_dbaccess.csv', benchmarkType);
    }
    else if (benchmarkType === 'binaryTrees') {
        benchmarkName = 'Binary Trees Benchmark';
        benchmarkDesc = 'Benchmark d\'arbres binaires';
        createViz('/ressources/measures/measures_binarytrees.csv', benchmarkType);
    }
    else if (benchmarkType === 'fileCompression') {
        benchmarkName = 'File compression Benchmark';
        benchmarkDesc = 'Benchmark de comrpession de fichiers';
        createViz('ressources/measures/measure_filecompression.csv', benchmarkType);
    }
    else if (benchmarkType === 'classification') {
        benchmarkName = 'Classification Benchmark';
        benchmarkDesc = 'Benchmark de classification de tweets';
        createViz('/ressources/measures/measures_classification.csv', benchmarkType);
    }
    else if (benchmarkType === 'fileTransfer') {
        benchmarkName = 'File Transfer Benchmark';
        benchmarkDesc = 'Benchmark de transfer de fichiers';
        createViz('/ressources/measures/measure_filetransfert.csv', benchmarkType);
    }
    document.getElementById('benchmarkName').innerText = benchmarkName;
    document.getElementById('benchmarkDesc').innerText = benchmarkDesc;
    CreateDefinition(benchmarkType);
}
;
