async function parseFile(filePath) {
    let out = new Array();
    await d3.dsv(';', filePath).then((values) => {
        for (let i = 0; i < values.length; i++) {
            let x = {};
            for (const [key, value] of Object.entries(values[i])) {
                if (key == 'cores-consumption' || key == 'cpu') {
                    x['cpu'] = value;
                }
                else if (key == 'ram-consumption' || key == 'ram') {
                    x['ram'] = value;
                }
                else {
                    x[key] = value;
                }
            }
            out.push(x);
        }
    });
    return out;
}
function sortDataIntoArrays(data) {
    let sortedArrays = {
        single: [],
        multiple: []
    };
    data.forEach((obj) => {
        switch (obj.mode) {
            case 'single':
                sortedArrays.single.push(obj);
                break;
            case 'multiple':
                sortedArrays.multiple.push(obj);
                break;
            default:
                break;
        }
    });
    return sortedArrays;
}
function normalizeData(value) {
    const max = Math.max(value['ram'], value['cpu']);
    const min = Math.min(value['ram'], value['cpu']);
    const normalizedData = {};
    Object.keys(value).forEach(key => {
        normalizedData[key] = map(value[key], 0, max, 0, 100);
    });
    return normalizedData;
}
function filterBy(values, key, value) {
    let out = new Array();
    for (let i = 0; i < values.length; i++) {
        const elem = values[i];
        if (elem[key] === value) {
            out.push(elem);
        }
    }
    return out;
}
function keyValues(values, key) {
    let out = new Array();
    for (let i = 0; i < values.length; i++) {
        const elem = values[i];
        const value = elem[key];
        if (value != undefined) {
            out.push(value);
        }
    }
    return out;
}
function getValueByKey(obj, key) {
    const entry = obj.find((entry) => Object.keys(entry)[0] === key);
    return entry ? Object.values(entry)[0] : null;
}
function averages(data, key) {
    let total = 0;
    for (let i = 0; i < data.length; i++) {
        const elem = data[i];
        total += parseFloat(elem[key]);
    }
    return (total / data.length).toFixed(2);
}
function averageObject(data, keys) {
    let out = new Array();
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        const value = averages(data, key);
        out.push({ [key]: value });
    }
    return out;
}
function sumAverageObject(data, keys) {
    let total = 0;
    data.forEach(obj => {
        for (const key in obj) {
            if (keys.includes(key)) {
                const numericValue = parseFloat(String(obj[key]));
                if (!isNaN(numericValue)) {
                    total += numericValue;
                }
            }
        }
    });
    return parseFloat(total.toFixed(2));
}
function conversion(data, multiplier) {
    let out = new Array();
    data.forEach(obj => {
        let newObj = {};
        for (const key in obj) {
            const numericValue = parseFloat(String(obj[key]));
            if (!isNaN(numericValue)) {
                newObj[key] = (numericValue / 3600000) * 0.001 * multiplier;
            }
        }
        newObj['label'] = obj['label'];
        out.push(newObj);
    });
    return out;
}
function map(x, in_min, in_max, out_min, out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
function createDatasetObject(obj, keys) {
    let out = {};
    for (let i = 0; i < keys.length; i++) {
        out[keys[i]] = averages(obj, keys[i]);
    }
    return out;
}
function normalizeDatasetObjects(objs, keys) {
    let out = new Array();
    objs.forEach((obj) => {
        out.push({});
    });
    keys.forEach((key) => {
        const values = [];
        objs.forEach((obj) => {
            values.push(obj[key]);
        });
        const max = Math.max(...values);
        objs.forEach((obj, index) => {
            const value = obj[key];
            out[index][key] = map(value, 0, max, 0, 1);
        });
    });
    return out;
}
function createDataForRadarChart(obj, keys, languages) {
    const labels = {
        'cpu': 'CPU Usage',
        'ram': 'RAM Usage',
        'sd': 'Disk Usage',
        'nic': 'Network Usage',
        'duration': 'Duration',
        'compression': 'Compression'
    };
    const backgroundColors = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 205, 86, 0.2)'
    ];
    const borderColors = [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 205, 86, 1)'
    ];
    const data = {
        labels: [],
        datasets: []
    };
    keys.forEach((key) => {
        data.labels.push(labels[key]);
    });
    obj.forEach((elem, i) => {
        const dataset = {
            label: languages[i],
            data: [],
            backgroundColor: backgroundColors[i],
            borderColor: borderColors[i],
            borderWidth: 1
        };
        keys.forEach((key) => {
            dataset.data.push(elem[key]);
        });
        data.datasets.push(dataset);
    });
    return data;
}
function createDataForBarChart(data1) {
    let labelsCustom = data1.map(item => item.label);
    let values = data1.map(item => parseFloat(item.value));
    const data = {
        labels: labelsCustom,
        datasets: [{
                axis: 'y',
                label: 'Dataset',
                data: values,
                fill: false,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 205, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgb(255, 99, 132)',
                    'rgb(255, 159, 64)',
                    'rgb(255, 205, 86)',
                    'rgb(75, 192, 192)',
                    'rgb(54, 162, 235)'
                ],
                borderWidth: 1
            }]
    };
    return data;
}
