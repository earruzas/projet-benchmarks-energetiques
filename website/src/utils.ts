declare let d3: any;

type DBMeasure = { ram: number, cpu: number, sd: number, nic: number, duration: number, mode: string };
type FileTransfertMeasure = {ram: number, cpu: number, sd: number, nic: number, duration: number };


async function parseFile<T>(filePath: string) : Promise<Array<T>> {
    let out = new Array<T>();

    await d3.dsv(';', filePath).then((values: any) => {
        for (let i = 0; i < values.length; i++) {
            let x: any = {};

            for (const [key, value] of Object.entries(values[i])) {
                if (key == 'cores-consumption' || key == 'cpu' ) {
                    x['cpu'] = value;
                } else if (key == 'ram-consumption' || key == 'ram') {
                    x['ram'] = value;
                } else {
                    x[key] = value;
                }
            }
            out.push(x as T);
        }
    });
    return out;
}

function sortDataIntoArrays(data: any) {
    let sortedArrays: { single: any[], multiple: any[] } = {
        single: [], 
        multiple: []
    };
  
    data.forEach((obj: any) => {
        switch (obj.mode) {
            case 'single':
                sortedArrays.single.push(obj);
                break;
            case 'multiple':
                sortedArrays.multiple.push(obj);
                break;
            default:
                break;
        }
    });
    return sortedArrays;
}

function normalizeData(value: any) : any {
    const max = Math.max(value['ram'], value['cpu']);
    const min = Math.min(value['ram'], value['cpu']);

    const normalizedData: { [key: string]: any } = {};
  
    Object.keys(value).forEach(key => {
        normalizedData[key] = map(value[key], 0, max, 0, 100);
    });

    return normalizedData;
}

function filterBy<T>(values: Array<T> , key: string, value: any) : Array<T> {
    let out = new Array<T>();

    for (let i = 0; i < values.length; i++) {
        const elem = values[i] as any;
        if (elem[key] === value) {
            out.push(elem);
        }
    }

    return out;
}

function keyValues<T, K>(values: Array<T>, key: string) : Array<K> {
    let out = new Array<K>();

    for (let i = 0; i < values.length; i++) {
        const elem = values[i] as any;
        const value = elem[key] as K;
        if (value != undefined) {
            out.push(value);
        }
    }

    return out;
}

function getValueByKey(obj: any, key: string) {
    const entry = obj.find((entry: any) => Object.keys(entry)[0] === key);
    return entry ? Object.values(entry)[0] : null;
}

function averages<T>(data: Array<T>, key: string) : number {
    let total = 0;
    for (let i = 0; i < data.length; i++) {
        const elem = data[i] as any;
        total += parseFloat(elem[key]);
    }
    return (total / data.length).toFixed(2) as any;
}

function averageObject<T>(data: Array<T>, keys: Array<string>) : Array<any> {
    let out = new Array<any>();

    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        const value = averages(data, key);
        out.push({ [key]: value });
    }

    return out;
}

function sumAverageObject<T>(data: Array<T>, keys: Array<string>) : number{
    let total = 0;

    data.forEach(obj => {
        for (const key in obj) {
            if (keys.includes(key)) {
                const numericValue = parseFloat(String(obj[key]));
                if (!isNaN(numericValue)) {
                    total += numericValue;
                }
            }
        }
    });
    return parseFloat(total.toFixed(2));
}

function conversion(data: Array<any>, multiplier: number): Array<any> {
    let out = new Array<any>();

    data.forEach(obj => {
        let newObj: any = {};
        for (const key in obj) {
            const numericValue = parseFloat(String(obj[key]));
            if (!isNaN(numericValue)) {
                newObj[key] = (numericValue / 3600000) * 0.001 * multiplier;
            }
        }
        newObj['label'] = obj['label'];
        out.push(newObj);

    });    

    return out
}


function map(x: number, in_min: number, in_max: number, out_min: number, out_max: number) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function createDatasetObject(obj: any, keys: Array<string>) : any {
    let out: any = {};

    for (let i = 0; i < keys.length; i++) {
        out[keys[i]] = averages(obj, keys[i]);
    }

    return out;
}

function normalizeDatasetObjects(objs: Array<any>, keys: Array<string>) : Array<any> {
    let out = new Array<any>();
    objs.forEach((obj) => {
        out.push({});
    });


    keys.forEach((key) => {
        const values: Array<number> = [];
        objs.forEach((obj) => {
            values.push(obj[key]);
        });

        const max = Math.max(...values);
    
        objs.forEach((obj, index) => {
            const value = obj[key];
            out[index][key] = map(value, 0, max, 0, 1);
        });

    });

    return out;
}

function createDataForRadarChart(obj: Array<any>, keys: Array<string>, languages: Array<string>) : any {
    const labels: { [key: string]: string; } = {
        'cpu': 'CPU Usage',
        'ram': 'RAM Usage',
        'sd': 'Disk Usage',
        'nic': 'Network Usage',
        'duration': 'Duration',
        'compression': 'Compression'
    }

    const backgroundColors = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 205, 86, 0.2)'

    ];

    const borderColors = [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 205, 86, 1)'
    ];

    const data: any = {
        labels: [],
        datasets: []
    };

    keys.forEach((key) => {
        data.labels.push(labels[key]);
    });

    obj.forEach((elem, i) => {
        const dataset: any = {
            label: languages[i],
            data: [],
            backgroundColor: backgroundColors[i],
            borderColor: borderColors[i],
            borderWidth: 1
        };

        keys.forEach((key) => {
            dataset.data.push(elem[key]);
        });

        data.datasets.push(dataset);
    });

    return data;
}

function createDataForBarChart(data1: Array<any>): any {    
    let labelsCustom = data1.map(item => item.label);
    let values = data1.map(item => parseFloat(item.value));
    
    const data = {
        labels: labelsCustom,
        datasets: [{
          axis: 'y',
          label: 'Dataset',
          data: values,
          fill: false,
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)'
          ],
          borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)'
          ],
          borderWidth: 1
        }]
      };

    return data; 
}
